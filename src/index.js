import React, { Suspense } from 'react'
import ReactDOM from 'react-dom'
import App from './components/container/App'
import * as serviceWorker from './serviceWorker'
import { Provider } from 'react-redux'
import reduxThunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import { createStore, applyMiddleware } from 'redux'
import reducers from './reducers'
import {
    HashRouter as Router
} from 'react-router-dom'
import './i18n'

const store = createStore(reducers, composeWithDevTools(applyMiddleware(reduxThunk)))

ReactDOM.render(
  <Suspense fallback={null}>
    <Provider store={store}>
        <Router>
            <App />
        </Router> 
    </Provider>
  </Suspense>,
    document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()