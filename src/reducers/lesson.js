import {    SET_LESSON_TITLE,
            SET_LESSON_DESC,
            SET_LESSON_MEDIA,
            SET_LESSON_TASKS,
            SET_LESSON_LOAD_FILE,
            SET_LESSON_IMAGE,
            SET_MIN_TIME,
            SET_MAX_TIME,
            SET_MODIFIED_AT,
            SET_LESSON_ID,
            SET_STATUS } from '../constants/'

const INITIAL = {
    title: '',
    description: '',
    media: [
        { 
            link: "", 
            media_type: "video"
        }
    ],
    tasks: {
        text: '',
        file: ''
    },
    loadFile: false,
    lessonImage: '',
    minTime: '',
    maxTime: '',
    modified_at: '',
    lessonId: '',
    status: ''
}

export default function lesson( state = INITIAL, action ) {
    switch (action.type) {
        case SET_LESSON_TITLE: 
            return { ...state, title: action.title }
        case SET_LESSON_DESC: 
            return { ...state, description: action.description }
        case SET_LESSON_MEDIA: 
            return { ...state, media: action.media }
        case SET_LESSON_TASKS: 
            return { ...state, tasks: action.tasks }
        case SET_LESSON_LOAD_FILE: 
            return { ...state, loadFile: !state.loadFile }
        case SET_LESSON_IMAGE: 
            return { ...state, lessonImage: action.lessonImage }
        case SET_MIN_TIME: 
            return { ...state, minTime: action.minTime }
        case SET_MAX_TIME: 
            return { ...state, maxTime: action.maxTime }
        case SET_MODIFIED_AT: 
            return { ...state, modified_at: action.modified_at }
        case SET_LESSON_ID: 
            return { ...state, lessonId: action.lessonId } 
        case SET_STATUS: 
            return { ...state, status: action.status } 
        default: 
            return state
    }
}