import { OPEN_MODAL, CLOSE_MODAL, SET_ADD_COURSE_INFO, SET_COURSE_ID, SET_COURSE_TITLE, SET_COURSE_DESCRIPTION } from '../constants/'

const INITIAL = {
    opened: false,
    modalName: null,
    addCourseInfo: {
        courseId: '',
        title: '',
        description: '',
        file: '',
        availability: false
    }   
}

export default function modal( state = INITIAL, action ) {
    switch (action.type) {
        case OPEN_MODAL:
            return { ...state, opened: true, modalName: action.modalName }
        case CLOSE_MODAL:
            return { ...state, opened: false, modalName: null }
        case SET_ADD_COURSE_INFO:
            return { ...state, addCourseInfo: state.addCourseInfo }
        case SET_COURSE_ID:
            let newState = state.addCourseInfo
            newState.courseId = action.courseId
            return { ...state, addCourseInfo: newState }
        case SET_COURSE_TITLE:
            let newState1 = state.addCourseInfo
            newState1.title = action.title
            return { ...state, addCourseInfo: newState1 }
        case SET_COURSE_DESCRIPTION:
            let newState2 = state.addCourseInfo
            newState2.description = action.description
            return { ...state, addCourseInfo: newState2 }
        default: 
            return state
    }
}