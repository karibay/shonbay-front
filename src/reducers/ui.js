import { SHOW_LEFT_MENU_MOBILE, SHOW_NOTIFICATION, HIDE_HEADER_LEFT_MENU } from '../constants/'

const INITIAL = {
    showLeftMenuMobile: false,
    notification: {
        show: false,
        text: 'Default text'
    },
    hideHeaderLeftMenu: false
}

export default function ui( state = INITIAL, action ) {
    switch (action.type) {
        case SHOW_LEFT_MENU_MOBILE:
            return { ...state, showLeftMenuMobile: !state.showLeftMenuMobile }
        case SHOW_NOTIFICATION:
            return { ...state, notification: !state.notificationInfo }
        case HIDE_HEADER_LEFT_MENU:
            return { ...state, hideHeaderLeftMenu: !state.hideHeaderLeftMenu }
        default: 
            return state
    }
}