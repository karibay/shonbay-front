import { combineReducers } from 'redux'
import userInfo from './userInfo'
import modal from './modal'
import ui from './ui'
import courses from './courses'
import lesson from './lesson'

const rootReducer = combineReducers({
    userInfo, modal, ui, courses, lesson
 })
 
 export default rootReducer
 