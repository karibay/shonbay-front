import {    SET_COURSE_LIST, 
            SET_COURSE_INFO, 
            SET_LESSON_LIST } from '../constants/'

const INITIAL = {
    courseList: [],
    courseInfo: {},
    lessonList: []
}

export default function courses( state = INITIAL, action ) {
    switch (action.type) {
        case SET_COURSE_LIST:
            return { ...state, courseList: action.courseList }
        case SET_COURSE_INFO:
            return { ...state, courseInfo: action.courseInfo }
        case SET_LESSON_LIST:
            return { ...state, lessonList: action.lessonList }
        default: 
            return state
    }
}