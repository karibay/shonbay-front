import {    LOGIN, 
            SET_USER_NAME, 
            SET_USER_SEX, 
            SET_USER_BDAY, 
            SET_USER_INSTAGRAM, 
            SET_USER_EMAIL, 
            SET_USER_PHONE, 
            SET_USER_PASSW, 
            SET_USER_REPEAT_PASSW,
            SET_USER_TOKEN,
            SET_USER_ID,
            SET_USER_TYPE } from '../constants/'

const INITIAL = {
    userId: '',
    token: '',
    username: '',
    sex: 1,
    bday: '',
    instagram: '',
    email: '',
    phone: '', //+77011231212
    passw: '',
    repeatPassw: '',
    usertype: ''
}

export default function userInfo( state = INITIAL, action ) {
    switch (action.type) {
        case LOGIN:
            return { ...state, username: action.username }
        case SET_USER_NAME:
            return { ...state, username: action.username }
        case SET_USER_SEX: 
            return { ...state, sex: action.sex }
        case SET_USER_BDAY: 
            return { ...state, bday: action.bday }
        case SET_USER_INSTAGRAM: 
            return { ...state, instagram: action.instagram }
        case SET_USER_EMAIL: 
            return { ...state, email: action.email }
        case SET_USER_PHONE: 
            return { ...state, phone: action.phone }
        case SET_USER_PASSW: 
            return { ...state, passw: action.passw }
        case SET_USER_REPEAT_PASSW: 
            return { ...state, repeatPassw: action.repeatPassw }
        case SET_USER_TOKEN: 
            return { ...state, token: action.token }
        case SET_USER_ID: 
            return { ...state, userId: action.userId }
        case SET_USER_TYPE: 
            return { ...state, usertype: action.usertype }
        default: 
            return state
    }
}