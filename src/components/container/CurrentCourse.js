import React, { Component } from 'react'
// import LeftMenu from '../container/LeftMenu'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClock, faPencilAlt, faCircleNotch, faCheck, faTimes, faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as modalActions from '../../actions/modal'
import * as courseActions from '../../actions/courses'
import Moment from 'react-moment'

const Item = (props) => {
    // taskCount, 
    let { title, deadline, status, goToLesson, goToEditLesson, goToDeleteLesson, userInfo, id } = props
    return (
        <div 
            onClick={(e) => {
                if ( status === 'blocked' || status === 'failed' ) {
                    alert('Курс заблокирован')
                } else {
                    goToLesson(e, id)
                }
            }} 
            className={ "row " + ( status === 'blocked' || status === 'failed' ? "" : "passed" ) }>
            <div className="name">
                {title}
            </div>
            <div className="time">
                <div className={ deadline ? "show" : "hide" }>
                    <FontAwesomeIcon icon={faClock} />
                    <Moment format="DD-MM-YYYY, hh:mm">{deadline}</Moment>
                </div>  
            </div>
            {
                ( status === 'submitted' ? <div className="status pass"><FontAwesomeIcon icon={faCheck} /> Өтілді</div> : <div></div> )
            }
            {
                ( status === 'blocked' ? 
                <div className="status">
                {/* <FontAwesomeIcon icon={faCheck} />  */}
                Заблокирован</div> : <div></div> )
            }
            {
                ( status === 'in_proccess' ? <div className="status process"><FontAwesomeIcon icon={faCircleNotch} /> В процессе</div> : <div></div> )
            }
            {
                ( status === 'available' ? 
                <div className="status pass">
                {/* <FontAwesomeIcon icon={faCircleNotch} />  */}
                Доступно</div> : <div></div> )
            }
            {
                ( status === 'failed' ? 
                <div className="status pass">
                {/* <FontAwesomeIcon icon={faCircleNotch} />  */}
                Провалено</div> : <div></div> )
            }
            <div className={"task " + (userInfo.usertype === 'admin' ? 'show' : 'hide' )}>
                <div style={{display: 'initial'}} onClick={(e) => {  if ( (status === 'blocked' || status === 'failed') && userInfo.usertype !== 'admin' ) { alert('Курс заблокирован') } 
                                        else { goToEditLesson(e, id) }}}>
                    <FontAwesomeIcon 
                        className={ ( (userInfo.usertype === 'admin') ? '' : 'hide' ) } 
                        icon={faPencilAlt} /> Өңдеу 
                </div>
                &nbsp;	&nbsp;	&nbsp;
                <div style={{display: 'initial'}} onClick={(e) => {  if ( (status === 'blocked' || status === 'failed') && userInfo.usertype !== 'admin'  ) { alert('Курс заблокирован') } 
                                        else { goToDeleteLesson(e, id) }}}>
                    <FontAwesomeIcon 
                        className={ "vermillion " + ( (userInfo.usertype === 'admin') ? '' : 'hide' ) } 
                        icon={faTrashAlt} /> <span className="vermillion">Өшіру</span>
                </div>
            </div>
        </div>
    )
}

class CurrentCourse extends Component {
    constructor(props) {
        super(props)
        this.state = {
            header: '',
            description: ''
        }
        this.goToLesson = this.goToLesson.bind(this)
        this.goToEditLesson = this.goToEditLesson.bind(this)
        this.goToDeleteLesson = this.goToDeleteLesson.bind(this)
    }
    goToLesson = (e, lessonId) => {
        const { courseId } = this.props.match.params
        this.props.history.push('/current-course/'+ courseId  +'/lesson/' + lessonId + '/' )
    }
    goToEditLesson = (e, lessonId) => {
        const { courseId } = this.props.match.params
        this.props.history.push('/current-course/'+ courseId  +'/edit-lesson/' + lessonId + '/' )
        // e.preventDefault()
        e.stopPropagation()
    }
    goToDeleteLesson = (e, lessonId) => {
        const { courseId } = this.props.match.params
        this.props.courseActions.DELETE_LESSON_REQUEST( courseId, lessonId, this.props.userInfo.token ).then((data) => {
            this.props.courseActions.GET_COURSE_LESSONS_REQUEST( courseId, this.props.userInfo.token ).then((data1) => {
                if (data1) {
                    this.props.courseActions.SET_LESSON_LIST( data1 )
                } else {
                    alert( 'Проблемы с сервером. Список уроков не обновлены.' )
                }
            })
        })
        e.stopPropagation()
    }
    componentDidMount() {
        const { courseId } = this.props.match.params
        const { token } = this.props.userInfo
        this.props.courseActions.GET_COURSE_LESSONS_REQUEST( courseId, token ).then((data) => {
            if (data) {
                this.props.courseActions.SET_LESSON_LIST( data )
            } else {
                alert( 'Проблемы с сервером' )
            }
        })

        this.props.courseActions.GET_COURSE_REQUEST( courseId, token ).then((data) => {
            if (data) {
                // console.log('data', data)
                this.setState({
                    header: data.title,
                    description: data.description
                })
            } else {
                alert( 'Проблемы с сервером. Невозможно получить информацию о курсе' )
            }
        })
    }
    render() {
        const { courseId } = this.props.match.params
        let { header, description } = this.state
        var elements = []
        for ( let i = 0; i < this.props.courses.lessonList.length; i++ ) {
            let item = this.props.courses.lessonList[i]
            elements.push( <Item    key={i}
                                    id={item.id} 
                                    title={item.title} 
                                    deadline={item.ends_at} 
                                    taskCount={item.tasks_count} 
                                    goToLesson={this.goToLesson} 
                                    goToEditLesson={this.goToEditLesson} 
                                    goToDeleteLesson={this.goToDeleteLesson} 
                                    userInfo={ this.props.userInfo }
                                    status={ item.status } /> )
        }
        return (
            <div style={{ display: 'flex' }}>
                {/* <LeftMenu /> */}
                <div className="current-course pl-40 pr-40 pt-40">
                    <div className="current-course__header fs-24 dark-indigo">
                        <div title={header} className="page-name fs-24">
                            {header}
                        </div>
                        <button onClick={ _ => this.props.history.push('/current-course/'+courseId+'/edit-lesson') } 
                                className={"add-course general-add-course ml-24 " + ( (this.props.userInfo.usertype === 'admin') ? 'show' : 'hide' )}>
                            <span className="fs-20 mr-10">+</span> <span>Жаңа сабақты қосу</span>
                        </button>
                        <div className={"notification ml-24 fs-14 " + (this.props.notification.show ? "show" : "hide")}>
                            { this.props.notification.text }
                            <FontAwesomeIcon className="notification__close" icon={faTimes} />
                        </div>
                        <div className="navigation fs-13 lh-16 blue-grey">
                            Курстар   |   {header}
                        </div>
                    </div>
                    <div className="current-course__description fs-14 slate">
                        <div title={description} className="page-description lh-20">
                            {description}
                        </div>
                    </div>
                    <div className="current-course__lesson-table mt-40">
                        <div className="current-course__lesson-table__head">
                            <div className="row fs-14 blue-grey">
                                <div className="name">
                                    Сабақ атауы
                                </div>
                                <div className="time"> 
                                    Уақыты
                                </div>
                                <div className="status">
                                    Мәртебе
                                </div>
                                <div className={"task " + (this.props.userInfo.usertype === 'admin' ? 'show' : 'hide' )}>
                                    Әрекеттер
                                </div>
                            </div>
                        </div>
                        <div className="current-course__lesson-table__body">
                            {elements} 
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    notification: state.ui.notification,
    courses: state.courses,
    userInfo: state.userInfo,
    lesson: state.lesson
})

const mapDispatchToProps = dispatch => ({
    modalActions: bindActionCreators(modalActions, dispatch),
    courseActions: bindActionCreators(courseActions, dispatch)
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(CurrentCourse))