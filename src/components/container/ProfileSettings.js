import React, { Component } from 'react'
import ProfileSettingsForm from '../presentational/ProfileSettingsForm'
// import { bindActionCreators } from 'redux'
// import * as userActions from '../../actions/user'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

class ProfileSettings extends Component {
    render() {
        return (
            <div style={{ display: 'flex' }}>
                <div className="profile-settings pl-40 pr-40 pt-40">
                    <div className="fs-24">
                        Профиль
                    </div>
                    <ProfileSettingsForm />
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    userInfo: state.userInfo
})

const mapDispatchToProps = dispatch => ({
    // userActions: bindActionCreators(userActions, dispatch)
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfileSettings))
