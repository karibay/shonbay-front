import React, { Component } from 'react'
import logo from '../../assets/img/shonbay_logo.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown, faChevronUp, faUser, faSignOutAlt, faBars } from '@fortawesome/free-solid-svg-icons'
import { faBell } from '@fortawesome/free-regular-svg-icons'
import avatar from '../../assets/img/32x32.png'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as uiActions from '../../actions/ui'
import LanguageSelector from '../presentational/LangSelector';

class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {
            mmenuOpen: false,
            dropdownState: [true, false]
        }
        this.changeIcon = this.changeIcon.bind(this)
    }
    changeIcon(event) {
        this.setState({ mmenuOpen: !this.state.mmenuOpen })
    }
    changeMenu(index) {
        var newState = [false,false]
        newState[index] = true
        this.setState({ dropdownState: newState })
        if ( index === 0 ) {
            this.props.history.push('/profile-settings')
        } else if ( index === 1 ) { // quit
            localStorage.removeItem( 'shonbay_token' )
            localStorage.removeItem( 'shonbay_user' )
            this.props.history.push('/login')
        }
    }
    render() {
        return (
            <div className="header">
                <header>
                    <div className="header__logo">
                        <Link className="item" to={`/courses`} >
                            <img alt="logo" src={logo}></img>
                        </Link>
                    </div>
                    <LanguageSelector />
                    <div className="header-mob__logo">
                        <FontAwesomeIcon onClick={() => this.props.uiActions.SHOW_LEFT_MENU()} icon={faBars} />
                    </div>
                    <div className="header__userInfo">
                        <div className="header__userInfo__text" style={{ marginRight:'26px'}}>
                        { this.props.userInfo.usertype === 'student' ? "Статус: Ученик" : "Статус: Администратор"  }
                        </div>

                        <div className="header__userInfo__notification">
                            <FontAwesomeIcon icon={faBell} />
                        </div>
                        <div className="header__userInfo__avatar-username">
                            <div className="avatar" onClick={() => this.changeIcon()}>
                                <img alt="avatar" src={avatar}></img>
                                <span className="username">{this.props.userInfo.username}</span>
                                <FontAwesomeIcon className="arrow" icon={ this.state.mmenuOpen ? faChevronUp : faChevronDown } />
                                <div className={"dropdown-menu " + (this.state.mmenuOpen ? 'show':'hide')}>
                                    <div onClick={() => this.changeMenu(0)} className={"item fs-14 " + (this.state.dropdownState[0] ? 'active':'')}>
                                        <label className="item-icon"><FontAwesomeIcon icon={faUser} /></label>
                                        Профиль
                                    </div>
                                    <div    onClick={() => this.changeMenu(1)} 
                                            className={"item fs-14 " + (this.state.dropdownState[1] ? 'active':'')}>
                                        <label className="item-icon"><FontAwesomeIcon icon={faSignOutAlt} /></label>
                                        Шығу
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    userInfo: state.userInfo
})

const mapDispatchToProps = dispatch => ({
    uiActions: bindActionCreators(uiActions, dispatch)
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(Header))