import React, { Component } from 'react'
// import logo from './logo.svg'
import '../../assets/style/index.css'
import Header from '../container/Header'
import Routing from '../Routing'
import Modal from './Modal'
import Footer from '../container/Footer'
import { library } from '@fortawesome/fontawesome-svg-core'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faIgloo } from '@fortawesome/free-solid-svg-icons'
import LeftMenu from './LeftMenu'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../../actions/user'

library.add(faIgloo)

class App extends Component {
  componentWillMount() {
    var token = localStorage.getItem( 'shonbay_token' )
    if ( token ) {
      this.props.userActions.SET_USER_TOKEN( token )
      this.props.userActions.SET_USER_ID( JSON.parse(localStorage.getItem( 'shonbay_user' )).id )
      this.props.userActions.SET_USER_PHONE( JSON.parse(localStorage.getItem( 'shonbay_user' )).phone_number )
      this.props.userActions.GET_CURRENT_USER_REQUEST( token ).then((data) => {
        if ( data ) {
          this.props.userActions.SET_USER_NAME( data.name )
          this.props.userActions.SET_USER_EMAIL( data.email )
          this.props.userActions.SET_USER_SEX( (data.gender === 'male') ? 1 : 0 )
          this.props.userActions.SET_USER_BDAY( data.birth_date )
          this.props.userActions.SET_USER_INSTAGRAM( data.instagram )
          this.props.userActions.SET_USER_TYPE( data.user_type )
        } else {
          alert('Проблемы с сервером')
        }
      })
    }
  }
  defineAvailableParts = (pathname) => {
    var auth = (pathname === '/login' || pathname === '/change-password' || pathname === '/email-code' || pathname === '/fill-profile') 
    var auth1 = (pathname === '/login' || pathname === '/change-password' || pathname === '/email-code') // exception for fill-profile
    var token = localStorage.getItem( 'shonbay_token' )
    // const { username, sex, bday, instagram } = this.props.userInfo
    // ===
    if ( token && auth1 ) { // if auth
      this.props.history.push( '/courses' )
    } else if ( !token && !auth ) { // if not
      this.props.history.push( '/login' )
    }
    // ===
    return auth
  }
  render() {
    const { pathname } = this.props.location
    const hide = this.defineAvailableParts(pathname)
    return (
      <div className="App">
        <div className={ this.props.isModalOpened ? 'show' : 'hide'}>
          <Modal />
        </div>
        
        <div className={ hide ? 'hide' : 'show' } style={{ position: 'fixed', width: '100%', height: '0', zIndex: '104' }}>
          <Header />
          <LeftMenu />
        </div>
        <Routing />
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
    isModalOpened: state.modal.opened,
    hideHeaderLeftMenu: state.ui.hideHeaderLeftMenu,
    userInfo: state.userInfo
})

const mapDispatchToProps = dispatch => ({
  userActions: bindActionCreators(userActions, dispatch)
})

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(App))