import React, { Component } from 'react'
import ModalWindow from '../presentational/ModalWindow'
import TaskFinishedModal from '../presentational/TaskFinishedModal'
import AddNewLessonModal from '../presentational/AddNewLessonModal'
import UpdateCourseModal from '../presentational/UpdateCourseModal'
import DeleteCourseModal from '../presentational/DeleteCourseModal'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

class Modal extends Component {
  render() {
    return (
      <div className="modal">
        <div className={ this.props.modalName === 'addCourse' ? 'show' : 'hide' }>
          <ModalWindow />
        </div>
        <div className={ this.props.modalName === 'editCourse' ? 'show' : 'hide' }>
          <UpdateCourseModal />
        </div>
        <div className={ this.props.modalName === 'sendTask' ? 'show' : 'hide' }>
          <TaskFinishedModal />
        </div>
        <div className={ this.props.modalName === 'addLesson' ? 'show' : 'hide' }>
          <AddNewLessonModal />
        </div>
        <div className={ this.props.modalName === 'deleteCourse' ? 'show' : 'hide' }>
          <DeleteCourseModal />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isModalOpened: state.modal.opened,
  modalName: state.modal.modalName
})

const mapDispatchToProps = dispatch => ({

})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(Modal))
