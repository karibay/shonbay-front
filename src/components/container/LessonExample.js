import React, { Component } from 'react'
// import LeftMenu from '../container/LeftMenu'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faVideo, faMusic, faDownload, faStopwatch, faPaperclip, faTimes } from '@fortawesome/free-solid-svg-icons'
// import img from '../../assets/img/img.jpg'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as modalActions from '../../actions/modal'
import * as courseActions from '../../actions/courses'
import * as lessonActions from '../../actions/lesson'
import ReactPlayer from 'react-player'
// import Moment from 'react-moment'
// import moment from "moment"

class LessonExample extends Component {
    constructor(props) {
        super(props)
        this.state = {
            anaswer: '',
            attachment: '',
            selectedVideo: 0,
            expireTime: ''
        }
        this.selectVideo = this.selectVideo.bind(this)
    }
    componentWillMount = () => {
        
    }
    componentWillUnmount = () => clearInterval(this.state.intervalId)
    selectVideo = (index) => this.setState({ selectedVideo: index })
    render() {
        let { lessonId, title, tasks, media, description } = this.props.lesson
        let { selectedVideo, expireTime } = this.state
        if ( !lessonId ) this.props.history.goBack()
        return (
            <div style={{ display: 'flex' }}>
                <div className="lesson pl-40 pr-40 pt-40">
                    <div className="lesson__header fs-24 dark-indigo">
                        <span className="page-name">
                            { title }
                        </span>
                        <div className={"notification ml-24 fs-14 " + (this.props.notification.show ? "show" : "hide")}>
                            { this.props.notification.text }
                            <FontAwesomeIcon className="notification__close" icon={faTimes} />
                        </div>
                        <div className="navigation fs-13 lh-16 blue-grey">
                            Курстар   |   Физиология   |   {lessonId}-сабақ
                        </div>
                    </div>
                    <div className="lesson__body mt-30">
                        <div className="lesson__body-row">
                            <div className="lesson__body-row__source">
                                <div className="lesson__body-row__source-img">
                                    {/* <img alt="video init" src={img}></img> */}
                                    <ReactPlayer 
                                        url={ media[0] ? (media[selectedVideo].link ? media[selectedVideo].link : "https://www.youtube.com/embed/4U_9aSvaRl0") : '"https://www.youtube.com/embed/4U_9aSvaRl0"' }  
                                        // playing
                                        width="100%"
                                        height="100%" />
                                </div>
                                <div className="lesson__body-row__source-list">
                                    {
                                        media.map((value, index) => {
                                            return (
                                                <button 
                                                    key={index}
                                                    className={"button lesson__body-row__source-list-button " + ( index !== 0 ? "ml-10 " : "" ) + ( index === selectedVideo ? "selected-button" : "" ) }
                                                    onClick={ _ => this.selectVideo(index) } >
                                                    <FontAwesomeIcon
                                                        className="video-icon" 
                                                        icon={ value.media_type === "video" ? faVideo : faMusic } /> <span className="fs-12 ml-8">
                                                            { value.media_type === "video" ? 'Видео' : 'Аудио' }
                                                        </span>
                                                </button>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                            <div className="lesson__body-row__task ml-20">
                                <div className="lesson__body-row__task-text slate fs-14">
                                    Тапсырма
                                    <div className="dark-indigo fs-14 lh-20 mt-12">
                                        { tasks.text }
                                    </div>
                                </div>
                                <div className="lesson__body-row__task-download">
                                    <button className={"button green-button lesson__body-row__task-download-button " + ( tasks.file ? 'show' : 'hide' ) }>
                                        <FontAwesomeIcon className="music-icon" icon={faDownload} /> 
                                        <span className="fs-12 ml-8">Файлды жүктеу</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="lesson__body-row mt-16">
                            <div className="lesson__body-row__advices slate fs-14">
                                <div>Мәтін</div>
                                <div dangerouslySetInnerHTML={{__html: description}} className="mt-16"></div>
                            </div>
                            <div className="lesson__body-row__task ml-20">
                                <div className="lesson__body-row__task__header">
                                    <div className="slate fs-14 lh-20">
                                        Есеп беру
                                    </div>
                                    <div className="orange-red fs-14 lh-20">
                                        <FontAwesomeIcon className="music-icon" icon={faStopwatch} />
                                        <span className="ml-8">{ expireTime }</span>
                                    </div>
                                </div>
                                <div className="lesson__body-row__task__body mt-18">
                                    <div style={{display: 'flex'}}>
                                        <textarea   
                                            value={this.state.text}
                                            onChange={ (e) => this.setState({ text: e.target.value })}
                                            placeholder="Берілген тапсырмалардын нәтижесін осында енгізіңіз. Қажетті файладры немесе суреттерді бекітіп тексеруге жіберіңіз."></textarea>
                                    </div>
                                    <div className="lesson__body-row__task__body__buttons mt-18">
                                        <input  style={{ position: 'absolute', visibility: 'hidden' }} 
                                                type="file" 
                                                id="my_file"
                                                onChange={ (data) => {
                                                    try {
                                                        let img, file = data.target.files[0]
                                                        img = new Image()
                                                        img.src = window.URL.createObjectURL(data.target.files[0])
                                                        const scope = this
                                                        img.onload = function() {
                                                            window.URL.revokeObjectURL(img.src)
                                                            scope.setState({ attachment: file })
                                                            alert( 'Картинка успешно загружена' )
                                                        }
                                                    } catch(error) {
                                                        alert( 'Картинка не загружена' )
                                                    }
                                            } }>
                                        </input>
                                        <button className="button">
                                            <FontAwesomeIcon className="music-icon" icon={faPaperclip} /> <span className="fs-12 ml-8">Файлды бекіту</span>
                                        </button>
                                        <button className={ "button selected-button fs-12 " + ( this.props.lesson.status === 'submitted' ? 'hide' : 'show' ) }>
                                            Жіберу
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    notification: state.ui.notification,
    userInfo: state.userInfo,
    lesson: state.lesson
})

const mapDispatchToProps = dispatch => ({
    modalActions: bindActionCreators(modalActions, dispatch),
    courseActions: bindActionCreators(courseActions, dispatch),
    lessonActions: bindActionCreators(lessonActions, dispatch)
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(LessonExample))