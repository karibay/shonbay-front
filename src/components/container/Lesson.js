import React, { Component } from 'react'
// import LeftMenu from '../container/LeftMenu'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faVideo, faMusic, faDownload, faStopwatch, faPaperclip, faTimes } from '@fortawesome/free-solid-svg-icons'
// import img from '../../assets/img/img.jpg'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as modalActions from '../../actions/modal'
import * as courseActions from '../../actions/courses'
import * as lessonActions from '../../actions/lesson'
import ReactPlayer from 'react-player'
// import Moment from 'react-moment'
import moment from "moment"

class Lesson extends Component {
    constructor(props) {
        super(props)
        this.state = {
            anaswer: '',
            attachment: '',
            selectedVideo: 0,
            expireTime: ''
        }
        this.downloadFile = this.downloadFile.bind(this)
        this.addFile = this.addFile.bind(this)
        this.selectVideo = this.selectVideo.bind(this)
        this.updateExpireTime = this.updateExpireTime.bind(this)
    }
    componentWillMount() {
        const { courseId, lessonId } = this.props.match.params
        this.props.courseActions.GET_LESSON_REQUEST( courseId, lessonId, this.props.userInfo.token ).then((data) => {
            if (data) {
                this.props.lessonActions.SET_LESSON_ID( data.id )
                this.props.lessonActions.SET_LESSON_TITLE( data.title )
                this.props.lessonActions.SET_LESSON_DESC( data.description )
                this.props.lessonActions.SET_LESSON_MEDIA( data.media )
                if ( data.media.length === 0 ) { this.props.lessonActions.SET_LESSON_MEDIA( [{ link: "", media_type: "video" }] ) }
                if ( data.task_file ) this.props.lessonActions.SET_LESSON_LOAD_FILE()
                this.props.lessonActions.SET_LESSON_TASKS( { text: data.tasks, file: data.task_file } )
                this.props.lessonActions.SET_LESSON_IMAGE( data.image )
                if ( data.starts_at ) this.props.lessonActions.SET_MIN_TIME( data.starts_at )
                if ( data.ends_at ) {
                    this.props.lessonActions.SET_MAX_TIME( data.ends_at )
                    // ======================================================
                    const intervalId = setInterval(() => this.updateExpireTime(data), 1000)
                    this.setState({ intervalId })
                    // ======================================================
                }
                this.props.lessonActions.SET_MODIFIED_AT( data.modified_at )
                this.props.lessonActions.SET_STATUS( data.status )
            } else { alert( 'Ошибка сервера' ) }
        })
    }
    componentWillUnmount = () => clearInterval(this.state.intervalId)
    downloadFile = (link) => window.open(  link , '_blank' )
    submitLesson = () => {
        const { courseId, lessonId } = this.props.match.params
        let formData = new FormData()
        let { answer, attachment } = this.state
        formData.append("answer", answer)
        formData.append("attachment", attachment)
        this.props.courseActions.SUBMIT_LESSON_REQUEST( formData, courseId, lessonId, this.props.userInfo.token ).then((data) => {
            if(data) { this.props.modalActions.OPEN_MODAL('sendTask') } 
            else { alert( 'Ошибка сервера' ) }
        })
    }
    addFile = () => { document.getElementById('my_file').click() }
    selectVideo = (index) => this.setState({ selectedVideo: index })
    updateExpireTime = ( data ) => {
        var now = moment().format("YYYY-MM-DDTHH:mm:ss+06:00")
        var expireDate = moment(data.ends_at).format("YYYY-MM-DDT23:59:00+06:00")
        var expiryTime = moment(expireDate).diff(moment(now))
        var formattedTime = moment.duration(expiryTime).format("DD күн, h:mm:ss")
        console.log( expiryTime )
        if ( expiryTime <= 0 ) {this.setState({ expireTime: 'Сабақ тапсыру уақыты өтіп кетті' }); clearInterval(this.state.intervalId);} 
        else { this.setState({ expireTime: formattedTime }) }
    }
    render() {
        let { lessonId, title, tasks, media, description } = this.props.lesson
        let { selectedVideo, expireTime } = this.state
        return (
            <div style={{ display: 'flex' }}>
                {/* <LeftMenu /> */}
                <div className="lesson pl-40 pr-40 pt-40">
                    <div className="lesson__header fs-24 dark-indigo">
                        <span className="page-name">
                            { title }
                        </span>
                        {/* <button className="add-course general-add-course ml-24">
                            <span className="fs-20 mr-10">+</span> <span>Курс қосу</span>
                        </button> */}
                        <div className={"notification ml-24 fs-14 " + (this.props.notification.show ? "show" : "hide")}>
                            { this.props.notification.text }
                            <FontAwesomeIcon className="notification__close" icon={faTimes} />
                        </div>
                        <div className="navigation fs-13 lh-16 blue-grey">
                            Курстар   |   Физиология   |   {lessonId}-сабақ
                        </div>
                    </div>
                    <div className="lesson__body mt-30">
                        <div className="lesson__body-row">
                            <div className="lesson__body-row__source">
                                <div className="lesson__body-row__source-img">
                                    {/* <img alt="video init" src={img}></img> */}
                                    <ReactPlayer 
                                        url={ media[0] ? (media[selectedVideo].link ? media[selectedVideo].link : "https://www.youtube.com/embed/4U_9aSvaRl0") : '"https://www.youtube.com/embed/4U_9aSvaRl0"' }  
                                        // playing
                                        width="100%"
                                        height="100%" />
                                </div>
                                <div className="lesson__body-row__source-list">
                                    {
                                        media.map((value, index) => {
                                            return (
                                                <button 
                                                    key={index}
                                                    className={"button lesson__body-row__source-list-button " + ( index !== 0 ? "ml-10 " : "" ) + ( index === selectedVideo ? "selected-button" : "" ) }
                                                    onClick={ _ => this.selectVideo(index) } >
                                                    <FontAwesomeIcon
                                                        className="video-icon" 
                                                        icon={ value.media_type === "video" ? faVideo : faMusic } /> <span className="fs-12 ml-8">
                                                            { value.media_type === "video" ? 'Видео' : 'Аудио' }
                                                        </span>
                                                </button>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                            <div className="lesson__body-row__task ml-20">
                                <div className="lesson__body-row__task-text slate fs-14">
                                    Тапсырма
                                    <div className="dark-indigo fs-14 lh-20 mt-12">
                                        { tasks.text }
                                    </div>
                                </div>
                                <div className="lesson__body-row__task-download">
                                    <button onClick={ _ => this.downloadFile(tasks.file) } 
                                            className={"button green-button lesson__body-row__task-download-button " + ( tasks.file ? 'show' : 'hide' ) }>
                                        <FontAwesomeIcon className="music-icon" icon={faDownload} /> 
                                        <span className="fs-12 ml-8">Файлды жүктеу</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="lesson__body-row mt-16">
                            <div className="lesson__body-row__advices slate fs-14">
                                <div>Мәтін</div>
                                <div dangerouslySetInnerHTML={{__html: description}} className="mt-16"></div>
                            </div>
                            <div className="lesson__body-row__task ml-20">
                                <div className="lesson__body-row__task__header">
                                    <div className="slate fs-14 lh-20">
                                        Есеп беру
                                    </div>
                                    <div className="orange-red fs-14 lh-20">
                                        <FontAwesomeIcon className="music-icon" icon={faStopwatch} />
                                        <span className="ml-8">{ expireTime }</span>
                                    </div>
                                </div>
                                <div className="lesson__body-row__task__body mt-18">
                                    <div style={{display: 'flex'}}>
                                        <textarea   
                                            value={this.state.text}
                                            onChange={ (e) => this.setState({ text: e.target.value })}
                                            placeholder="Берілген тапсырмалардын нәтижесін осында енгізіңіз. Қажетті файладры немесе суреттерді бекітіп тексеруге жіберіңіз."></textarea>
                                    </div>
                                    <div className="lesson__body-row__task__body__buttons mt-18">
                                        <input  style={{ position: 'absolute', visibility: 'hidden' }} 
                                                type="file" 
                                                id="my_file"
                                                onChange={ (data) => {
                                                    try {
                                                        let img, file = data.target.files[0]
                                                        img = new Image()
                                                        img.src = window.URL.createObjectURL(data.target.files[0])
                                                        const scope = this
                                                        img.onload = function() {
                                                            window.URL.revokeObjectURL(img.src)
                                                            scope.setState({ attachment: file })
                                                            alert( 'Картинка успешно загружена' )
                                                        }
                                                    } catch(error) {
                                                        alert( 'Картинка не загружена' )
                                                    }
                                            } }>
                                        </input>
                                        <button onClick={ this.addFile } className="button">
                                            <FontAwesomeIcon className="music-icon" icon={faPaperclip} /> <span className="fs-12 ml-8">Файлды бекіту</span>
                                        </button>
                                        <button onClick={ _ => this.submitLesson() } 
                                                className={ "button selected-button fs-12 " + ( this.props.lesson.status === 'submitted' ? 'hide' : 'show' ) }>
                                            Жіберу
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    notification: state.ui.notification,
    userInfo: state.userInfo,
    lesson: state.lesson
})

const mapDispatchToProps = dispatch => ({
    modalActions: bindActionCreators(modalActions, dispatch),
    courseActions: bindActionCreators(courseActions, dispatch),
    lessonActions: bindActionCreators(lessonActions, dispatch)
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(Lesson))