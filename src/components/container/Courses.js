import React, { Component } from 'react'
import CoursesList from '../presentational/CoursesList'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as modalActions from '../../actions/modal'
import { withTranslation } from 'react-i18next';

// import * as uiActions from '../../actions/ui'

class Courses extends Component {
    render() {
        const { t } = this.props;
        return (
            // <div>
                <div className="courses pl-40 pr-40 pt-40">
                    <div className="courses__header fs-24 dark-indigo">
                        <span className="page-name">
                            { t('coursesTitle') }
                        </span>
                        <button onClick={ _ => this.props.modalActions.OPEN_MODAL('addCourse') } 
                        className={"add-course general-add-course ml-24 " + ( (this.props.userInfo.usertype === 'admin') ? 'show' : 'hide' ) }>
                            <span className="fs-20 mr-10">+</span> <span>{ t('addCourseButtonShortText') }</span>
                        </button>
                        <div className={"notification ml-24 fs-14 " + (this.props.notification.show ? "show" : "hide")}>
                            { this.props.notification.text }
                            <FontAwesomeIcon className="notification__close" icon={faTimes} />
                        </div>
                        <div className="navigation fs-13 lh-16 blue-grey">
                            { t('coursesTitle') }   |    Физиология
                        </div>
                    </div>
                    <CoursesList />
                </div>
            // </div>
        )
    }
}

const mapStateToProps = state => ({
    notification: state.ui.notification,
    userInfo: state.userInfo
})

const mapDispatchToProps = dispatch => ({
    modalActions: bindActionCreators(modalActions, dispatch),
    // uiActions: bindActionCreators(uiActions, dispatch)
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(withTranslation()(Courses)))