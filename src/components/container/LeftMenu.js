import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBookmark, faExclamationTriangle, faNewspaper } from '@fortawesome/free-solid-svg-icons'

class LeftMenu extends Component {
    constructor(props) {
        super(props)
        this.state = {
            menuArray: [false, false, false],
            showMenu: false
        }
        this.handleMenuChange = this.handleMenuChange.bind(this)
        this.menuTrigger = this.menuTrigger.bind(this)
    }
    componentWillMount() {
        const { pathname } = this.props.location
        if ( pathname === '/courses' || pathname === '/current-course' ) {
            this.handleMenuChange(0)
        } else if ( pathname === '/rules' ) {
            this.handleMenuChange(1)
        } else if ( pathname === '/rules' ) {
            this.handleMenuChange(2)
        }
    }
    handleMenuChange = (choosenIndex) => {
        var newState = [false, false, false]
        newState[choosenIndex] = true
        this.setState({ menuArray: newState })
        // console.log( choosenIndex )
        if ( choosenIndex === 0 ) {
            this.props.history.push('/courses')
        } else if ( choosenIndex === 1 ) {
            window.open( './journal.html', '_blank' )
            // this.props.history.push('/rules')
        } else if ( choosenIndex === 2 ) {
            this.props.history.push('/rules')
        }
    }
    menuTrigger = () => {
        
    }
    render() {
        return (
            <div>
                <div className="left-menu">
                    <div className={"select-item fs-13 pt-18 pb-18 " + (this.state.menuArray[0] ? 'active' : 'inactive')}
                        onClick={() => this.handleMenuChange(0)}>
                        <FontAwesomeIcon icon={faBookmark} /> <span className="ml-8">Курстар</span>
                    </div>
                    <div className={"select-item fs-13 pt-18 pb-18 " + (this.state.menuArray[1] ? 'active' : 'inactive')}
                        onClick={() => this.handleMenuChange(1)}>
                        <FontAwesomeIcon icon={faNewspaper} /> <span className="ml-8">Журнал</span>
                    </div>
                    <div className="left-menu__footer">
                        <div className={"select-item fs-13 pt-18 pb-18 " + (this.state.menuArray[2] ? 'active' : 'inactive')}
                            onClick={() => this.handleMenuChange(2)}>
                            <FontAwesomeIcon icon={faExclamationTriangle} /> <span className="ml-8">Ережелер</span>
                        </div>
                        <div className="select-item fs-12 pt-18 pb-18">
                            ©2019 - ТОО “Shonbay Online University”.
                        </div>
                    </div>
                </div>

                {/* MOBILE */}
                <div className={"left-menu mobile " + (this.props.showLeftMenuMobile ? 'show' : 'hide')}>
                    <div className={"select-item fs-13 pt-18 pb-18 " + (this.state.menuArray[0] ? 'active' : 'inactive')}
                        onClick={() => this.handleMenuChange(0)}>
                        <FontAwesomeIcon icon={faBookmark} /> <span className="ml-8">Курстар</span>
                    </div>
                    <div className={"select-item fs-13 pt-18 pb-18 " + (this.state.menuArray[1] ? 'active' : 'inactive')}
                        onClick={() => this.handleMenuChange(1)}>
                        <FontAwesomeIcon icon={faNewspaper} /> <span className="ml-8">Журнал</span>
                    </div>
                    <div className="left-menu__footer">
                        <div className={"select-item fs-13 pt-18 pb-18 " + (this.state.menuArray[2] ? 'active' : 'inactive')}
                            onClick={() => this.handleMenuChange(2)}>
                            <FontAwesomeIcon icon={faExclamationTriangle} /> <span className="ml-8">Ережелер</span>
                        </div>
                        <div className="select-item fs-12 pt-18 pb-18">
                            ©2019 - ТОО “Shonbay Online University”.
                        </div>
                    </div>
                </div>

            </div>
            
        )
    }
}

const mapStateToProps = state => ({
    showLeftMenuMobile: state.ui.showLeftMenuMobile
})

const mapDispatchToProps = dispatch => ({
    
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(LeftMenu))
