import React, { Component } from 'react'
// import LeftMenu from '../container/LeftMenu'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faVideo, faMusic, faLink, faPlus, faPaperclip, faSave, faEye, faChevronDown, faImage, faTimes, faTrashAlt } from '@fortawesome/free-solid-svg-icons'
// , faMusic, faDownload, faStopwatch, faPaperclip
// import img from '../../assets/img/img.jpg'
import { Editor } from '@tinymce/tinymce-react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as modalActions from '../../actions/modal'
import * as courseActions from '../../actions/courses'
import * as lessonActions from '../../actions/lesson'
import Moment from 'react-moment'
import moment from "moment"
import InputMask from 'react-input-mask'

export class ConvertTime extends Component {
    render() {
        return (
            <Moment format="YYYY/MM/DD">1976-04-19T12:59-0500</Moment>
        )
    }
}

class EditLesson extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dropdownOpened: false,
            choices: [ 
                {value: 'Мерзім жоқ', selected: false},
                {value: 'Мерзім бар', selected: true},
            ],
            selectedChoice: '',
            downloadFile: false
        }
        this.dropdownEventHandle = this.dropdownEventHandle.bind(this)
        this.selectChoice = this.selectChoice.bind(this)
        // this.changeToggle = this.changeToggle.bind(this)
        this.downloadFile = this.downloadFile.bind(this)
        this.addNewLink = this.addNewLink.bind(this)
        this.setLinkToMedia = this.setLinkToMedia.bind(this)
        this.updateTasks = this.updateTasks.bind(this)
        this.addFile = this.addFile.bind(this)
        this.saveLesson = this.saveLesson.bind(this)
        this.setMediaType = this.setMediaType.bind(this)
        this.watchLessonExample = this.watchLessonExample.bind(this)
        this.changeLessonImage = this.changeLessonImage.bind(this)
    }
    componentWillMount() {
        this.selectChoice(0)
        const { courseId, lessonId } = this.props.match.params
        if ( lessonId ) {
            this.props.courseActions.GET_LESSON_REQUEST( courseId, lessonId, this.props.userInfo.token ).then((data) => {
                if (data) {
                    this.props.lessonActions.SET_LESSON_ID( data.id )
                    this.props.lessonActions.SET_LESSON_TITLE( data.title )
                    this.props.lessonActions.SET_LESSON_DESC( data.description )
                    this.props.lessonActions.SET_LESSON_MEDIA( data.media )
                    if ( data.media.length === 0 ) {
                        this.props.lessonActions.SET_LESSON_MEDIA( 
                            [
                                { 
                                    link: "", 
                                    media_type: "video"
                                }
                            ] 
                        )
                    }
                    if ( data.task_file ) this.props.lessonActions.SET_LESSON_LOAD_FILE()
                    this.props.lessonActions.SET_LESSON_TASKS( { text: data.tasks, file: data.task_file } )
                    this.props.lessonActions.SET_LESSON_IMAGE( data.image )
                    if ( data.starts_at ) this.props.lessonActions.SET_MIN_TIME( data.starts_at )
                    if ( data.ends_at ) {
                        this.props.lessonActions.SET_MAX_TIME( data.ends_at )
                        this.selectChoice(1)
                    }
                    this.props.lessonActions.SET_MODIFIED_AT( data.modified_at )
                    this.props.lessonActions.SET_STATUS( data.status )
                } else {
                    alert('Ошибка с сервером')
                }
            })
        }
    }
    dropdownEventHandle = () => this.setState({ dropdownOpened: !this.state.dropdownOpened })
    selectChoice = (selectedIndex) => {
        var newState = this.state.choices.map((newValue, newIndex) => {
            if ( newIndex === selectedIndex ) {
                newValue.selected = true
                this.setState({ selectedChoice: newValue.value })
                return newValue
            }
            newValue.selected = false
            return newValue
        })
        this.setState({ choices: newState })
    }
    // changeToggle = () => { this.setState({ downloadFile: !this.state.downloadFile }) }
    // handleEditorChange = (e) => { console.log('Content was updated:', e.target.getContent()) }
    downloadFile = () => window.open(  '/download.pdf' , '_blank' )
    addNewLink = () => {
        var media = this.props.lesson.media
        if ( media.length < 5 ) {
            media.push(
                { 
                    link: "", 
                    media_type: "video"
                }
            )
            this.props.lessonActions.SET_LESSON_MEDIA( media )
        } else { alert( 'Прикрепить можно не больше 5 ссылок' ) }
    }
    setLinkToMedia = (e, index) => {
        let result = this.props.lesson.media
        result[index].link = e.target.value
        this.props.lessonActions.SET_LESSON_MEDIA( result )
    }
    isValidURL(str) {
        const pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
          '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
          '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
          '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
          '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
          '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        return !!pattern.test(str);
    }
    updateTasks = (e) => {
        let result = this.props.lesson.tasks
        result.text = e.target.value
        this.props.lessonActions.SET_LESSON_TASKS( result )
    }
    addFile = () => document.getElementById('my_file').click()
    saveLesson = () => { 
        const { courseId, lessonId } = this.props.match.params
        let { title, description, media, tasks, loadFile, lessonImage, minTime, maxTime } = this.props.lesson
        media = media.filter(item => item.link.length !== 0)
        let formData = new FormData()
        formData.append("title", title)
        formData.append("description", description)
        formData.append("media", media)
        formData.append("tasks", tasks.text)
        if (loadFile && (typeof tasks.file !== 'string')) { formData.append("task_file", tasks.file) }
        if (typeof lessonImage !== "string") formData.append("image", lessonImage)
        if ( !(this.state.selectedChoice.localeCompare('Мерзім жоқ') === 0) ) {  
            formData.append("starts_at", moment(minTime).format("YYYY-MM-DDT00:00:00") )
            formData.append("ends_at", moment(maxTime).format("YYYY-MM-DDT00:00:00"))
        }
        formData.append("course", 1)
        formData.append("task_count", 1)
        if ( lessonId ) { // edit
            let linkHasErrors = media.filter((item) => {
                return item.link.length !== 0 && !this.isValidURL(item.link)
            })
            console.log(linkHasErrors.length !== 0)
            if (linkHasErrors.length !== 0) {
                alert('Сілтеме қате енгізілді')
                return
            }
            this.props.courseActions.UPDATE_LESSON_REQUEST( formData, courseId, lessonId, this.props.userInfo.token ).then((data) => {
                console.log(media)
                if (data) {
                    alert( 'Урок обновлен' )
                    this.props.courseActions.UPDATE_MEDIA_REQUEST( media, courseId, lessonId, this.props.userInfo.token ).then((data) => {
                        if (data) {
                            alert( 'Видео сохранено' )
                        } else {
                            alert( 'Видео не сохранено' )
                        }
                    })
                    this.props.history.push('/current-course/' + courseId + '/')
                } else {
                    alert( 'Ошибка, урок не сохранился.' )
                }
            })
        } else { // create
            this.props.courseActions.CREATE_COURSE_LESSON_REQUEST( courseId, formData, this.props.userInfo.token ).then((data) => {
                if (data) {
                    alert( 'Урок сохранен' )
                    this.props.history.push('/current-course/' + courseId + '/')
                } else {
                    alert( 'Ошибка, урок не сохранился.' )
                }
            })
        }
    }
    setMediaType = (value, index) => {
        let media = this.props.lesson.media
        media[index].media_type = (value.media_type === 'audio' ? 'video' : 'audio' )
        this.props.lessonActions.SET_LESSON_MEDIA( media )
    }
    watchLessonExample = () => this.props.history.push('/lesson-example')
    changeLessonImage = () => {
        this.props.lessonActions.SET_LESSON_IMAGE( "" )
    }
    render() {
        return (
            <div style={{ display: 'flex' }}>
                {/* <LeftMenu /> */}
                <div className="edit-lesson pl-40 pr-40 pt-40">
                    <div className="edit-lesson__header fs-24 dark-indigo">
                        <span className="page-name dark-indigo">
                            Сабақты редакциялау
                        </span>
                        <div className={"notification ml-24 fs-14 " + (this.props.notification.show ? "show" : "hide")}>
                            { this.props.notification.text }
                            <FontAwesomeIcon className="notification__close" icon={faTimes} />
                        </div>
                        <div className="navigation fs-13 lh-16 blue-grey">
                            Курстар   |    Физиология   |   Жаңа сабақ қосу
                        </div>
                    </div>
                    <div className="edit-lesson__body mt-30">
                        <div className="edit-lesson__body__column">
                            <div style={{display: 'flex'}}>
                                <input  className="input"
                                        placeholder="Сабақ тақырыбын енгізіңіз"
                                        value={this.props.lesson.title}
                                        onChange={(e) => this.props.lessonActions.SET_LESSON_TITLE( e.target.value )} ></input>
                            </div>
                            <div className="edit-lesson__body__column-editor mt-16">
                                <Editor
                                    initialValue={"<p>"+ this.props.lesson.description +"</p>"}
                                    init={{
                                        plugins: 'link image code',
                                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code',
                                        resize: false,
                                        height: "307px"
                                    }}
                                    onChange={(e) => this.props.lessonActions.SET_LESSON_DESC( e.target.getContent() )} 
                                />
                            </div>
                            <div className="edit-lesson__body__column__video-links">
                                <div className="fs-14 slate lh-20">
                                    Сабақтын видео және аудиосы
                                </div>
                                <div className="blue-grey fs-13 mt-8">
                                    Сабаққа бірнеше видео/аудио қоса аласыз. Төмендегі өріске видео/аудио сілтемесін енгізіңіз.
                                </div>
                                {
                                    this.props.lesson.media.map((value, index) => {
                                        return (
                                            <div key={index} className="edit-lesson__body__column__video-links-action">
                                                <button 
                                                        onClick={ _ => this.setMediaType(value, index) } 
                                                        className="button">
                                                    <FontAwesomeIcon icon={ value.media_type === 'audio' ? faMusic : faVideo} />
                                                </button>
                                                <div className="edit-lesson__body__column__video-links-action__wrap" style={{position: 'relative'}}>
                                                    <FontAwesomeIcon className="link-icon" icon={faLink} />
                                                    <input  className={`input ml-16 ${this.props.showLinkError ? "errored" : ""}`}
                                                            placeholder="Видеонын сілтемесін енгізіңіз"
                                                            value={value.link}
                                                            onChange={ (e) => this.setLinkToMedia(e, index) }></input>
                                                </div>
                                                <button onClick={ _ => this.addNewLink() } 
                                                        className={"button selected-button ml-16 " + ((index + 1) === this.props.lesson.media.length ? '' : 'hide') }>
                                                    <FontAwesomeIcon icon={faPlus} />
                                                </button>
                                            </div>
                                        )
                                    })
                                }
                               
                            </div>
                            <div className="edit-lesson__body__column-tasks mt-16">
                                <div className="slate fs-14 lh-20">
                                    Тапсырмалар
                                </div>
                                <div className="blue-grey fs-13 mt-8">
                                    Төмендегі өріске тапсырманын мәтінін толтырыңыз. Тапсырмаға байланысты файлдар бар болса, “Файлды бекіту” түймесіне басыңыз.
                                </div>
                                <textarea 
                                    placeholder="Мәтінді теріңіз"
                                    value={ this.props.lesson.tasks.text }
                                    onChange={ this.updateTasks } >
                                </textarea>
                                <div    className={ this.props.lesson.loadFile ? '':'hide' } 
                                        style={{position: 'relative' , display: 'inline-block'}}>
                                    <input  style={{ position: 'absolute', visibility: 'hidden' }} 
                                            type="file" 
                                            id="my_file"
                                            onChange={ (data) => {
                                                try {
                                                    let img, file = data.target.files[0]
                                                    img = new Image()
                                                    img.src = window.URL.createObjectURL(data.target.files[0])
                                                    const scope = this
                                                    img.onload = function() {
                                                        window.URL.revokeObjectURL(img.src)
                                                        var result = scope.props.lesson.tasks
                                                        result.file = file
                                                        scope.props.lessonActions.SET_LESSON_TASKS(result)
                                                        alert( 'Картинка успешно загружена' )
                                                    }
                                                } catch(error) {
                                                    alert( 'Картинка не загружена' )
                                                }
                                            } }>
                                    </input>
                                    <button className="button mt-16"
                                            onClick={ this.addFile }>
                                        <FontAwesomeIcon icon={faPaperclip} /> <span className="ml-8">Файлды бекіту</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="edit-lesson__body__column ml-16">
                            <div className="edit-lesson__body__column-save">
                                <div className="slate fs-14 lh-20">
                                    Жариялау
                                </div>
                                <div className="edit-lesson__body__column-save-buttons">
                                    <button onClick={ _ => this.saveLesson() } 
                                            className="button selected-button">
                                        <FontAwesomeIcon icon={faSave} /> <span className="ml-8">Сақтау</span>
                                    </button>
                                    <button className="button ml-16"
                                            onClick={ _ => this.watchLessonExample() }>
                                        <FontAwesomeIcon icon={faEye} /> <span className="ml-8">Көру</span>
                                    </button>
                                </div>
                                <div className="fs-12 light-grey-blue mt-12">
                                    <span className="mr-10">Соңғы жариялау:</span>  
                                    <Moment format="YYYY/MM/DD, hh:mm">
                                        { this.props.lesson.modified_at }
                                    </Moment>
                                </div>
                            </div>
                            
                            <div className="edit-lesson__body__column-expiry mt-16">
                                <div className="fs-14 lh-20 slate">
                                    Есеп беру баптаулары
                                </div>
                                <div className="mt-20 blue-grey fs-12">
                                    Ақырғы мерзім
                                </div>
                                <div className="dropdown-menu mt-6">
                                    <div className="dropdown-menu__wrap" onClick={() => this.dropdownEventHandle()}>
                                        <FontAwesomeIcon className="fa-chevron-down" icon={faChevronDown} />
                                        <input  type="text"
                                                value={this.state.selectedChoice}
                                                className="input fs-14 dark-indigo" 
                                                disabled
                                                    />
                                        <div className={"dropdown-menu__wrap-menu " + (this.state.dropdownOpened ? 'show' : 'hide')}>
                                            <ul>
                                                {this.state.choices.map((choice, index) =>
                                                    <li key={choice.value} 
                                                        className={ choice.selected ? "fs-14 dark-indigo selected" : "fs-14 dark-indigo"}
                                                        onClick={() => this.selectChoice(index)} >
                                                        {choice.value}
                                                    </li>
                                                )} 
                                            </ul>
                                        </div>
                                    </div> 
                                    <div className={ (this.state.selectedChoice.localeCompare('Мерзім жоқ') === 0) ? 'hide' : '' } style={{ display: 'flex' }}>
                                        <InputMask
                                            // {...this.props}
                                            placeholder="Бастапқы"
                                            value={ this.props.lesson.minTime }
                                            onChange={ (e) => this.props.lessonActions.SET_MIN_TIME( e.target.value ) }
                                            mask="9999-99-99" 
                                            maskChar="_"
                                            className="input fs-14 mt-10 dark-indigo" 
                                            alwaysShowMask={false} />
                                             
                                        <InputMask
                                            // {...this.props}
                                            placeholder="Соңғы"
                                            value={ this.props.lesson.maxTime }
                                            onChange={ (e) => this.props.lessonActions.SET_MAX_TIME( e.target.value ) }
                                            mask="9999-99-99" 
                                            maskChar="_"
                                            className="input fs-14 mt-10 dark-indigo" 
                                            alwaysShowMask={false} />
                                    </div>
                                                                       
                                </div>
                                <div className="mt-20 blue-grey fs-12">
                                    Файлды жүктеу
                                </div>
                                <div className="download-file-toggle">
                                    <label className="switch">
                                        <input  type="checkbox" 
                                                checked={this.props.lesson.loadFile} />
                                        <span   className="slider round"
                                                onClick={ _ => this.props.lessonActions.SET_LESSON_LOAD_FILE()}></span>
                                    </label>
                                    <div className="ml-14 slate fs-14">
                                        Ия
                                    </div>
                                </div>
                            </div>

                            <div className="edit-lesson__body__column-clickbate mt-16">
                                <div className="fs-14 lh-20 blue-grey">
                                    Сабақтын суреті
                                </div>
                                <div className="mt-8 blue-grey fs-12">
                                    Сабақтың видео немесе аудиосы болмаған жағдайда, осы сурет қолданылады.
                                </div>
                                <div className={ "image-filled mt-16 " + ((this.props.lesson.lessonImage !== '') ? 'show' : 'hide')} >
                                    <img className={ (typeof this.props.lesson.lessonImage === "string") ? "show" : "hide" } src={this.props.lesson.lessonImage}></img>
                                    {/* <div className="mt-8 blue-grey fs-12">
                                        Суретті ауыстыру және жаңарту үшін, үстіне басыңыз.
                                    </div> */}
                                    <button className="button vermillion mt-16"
                                            onClick={ _ => this.changeLessonImage() }>
                                        <FontAwesomeIcon icon={faTrashAlt} />
                                        <span className="ml-10">Суретті ауыстыру</span>
                                    </button>
                                </div>
                                <div className={ (this.props.lesson.lessonImage === '') ? 'show' : 'hide' }>
                                    <div className="set-clickbate" >
                                        <div style={{position: 'relative'}}>
                                            <FontAwesomeIcon className="fa-image" icon={faImage} />
                                            <input  type="file" 
                                                    className="input" 
                                                    placeholder="Суретті жүктеніз"
                                                    onChange={ (data) => {
                                                        try {
                                                            let img, file = data.target.files[0]
                                                            img = new Image()
                                                            img.src = window.URL.createObjectURL(data.target.files[0])
                                                            const scope = this
                                                            img.onload = function() {
                                                                window.URL.revokeObjectURL(img.src)
                                                                scope.props.lessonActions.SET_LESSON_IMAGE(file)
                                                                alert( 'Картинка успешно загружена' )
                                                            }
                                                        } catch(error) {
                                                            alert( 'Картинка не загружена' )
                                                        }
                                                    } } ></input>
                                        </div>
                                        <button className="button selected-button ml-16">
                                            <FontAwesomeIcon icon={faPaperclip} />
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    notification: state.ui.notification,
    courses: state.courses,
    lesson: state.lesson,
    userInfo: state.userInfo
})

const mapDispatchToProps = dispatch => ({
    modalActions: bindActionCreators(modalActions, dispatch),
    courseActions: bindActionCreators(courseActions, dispatch),
    lessonActions: bindActionCreators(lessonActions, dispatch)
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(EditLesson))