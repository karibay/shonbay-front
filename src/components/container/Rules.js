import React, { Component } from 'react'
// import LeftMenu from '../container/LeftMenu'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faClock, faPencilAlt, faCircleNotch, faCheck } from '@fortawesome/free-solid-svg-icons'

class Rules extends Component {
    render() {
        return (
            <div style={{ display: 'flex' }}>
                {/* <LeftMenu /> */}
                <div className="rules pl-40 pr-40 pt-40">
                    <div className="rules__header fs-24 dark-indigo">
                        <div className="page-name fs-24">
                            Ережелер
                        </div>
                    </div>
                   <div className="rules__main-block mt-24">
                        <div className="rules__main-block__header fs-22 mb-18 lh-20">
                            Курс қалай өтеді?
                        </div>
                        <div className="rules__main-block__text lh-20 fs-14">
                            Университет аясындағы ақпарат, тапсырмалар, тапсырманың есебін беру, сабақ кестесі, жаңалықтарды оқу ыңғайлы болу үшін, арнайы сайт (алдығы уақытта платформа) құрастырдық. Барлық сабақ тек сол платформа арқылы жүреді. Сілтемесі: https://shou.kz/
                            <br/><br/>
                            Оқу басталатын күні, бізге жіберген e-mail почтаңызға, немесе телефоныңызға смс түрінде платформаға кіруге қажетті «құпия сөзі» бар хат жібереміз. Сондықтан Ақпаратты ДҰРЫС жіберу өте маңызды! Ақпарат қате жіберілген жағдайда, платформаға уақытылы кіргеніңізге жауап бермейміз.
                            <br/><br/>
                            Айлық төлем жасап, қатысып жатқан қатысушылар, сіздер платформаға әр айға доступ аласыздар. Төлем жасалмаған жағдайда, келесі айда курс сізге қолжетімсіз болады!
                            <br/><br/>
                            Кейін почтаңызға ешнәрсе келмейді. Барлық сабақ тек платформа аясында болады. 
                            Платформаға 3-тен артық техника құралдары арқылы кірсеңіз, курс қолжетімсіз болып қалатынын ескертеміз.
                            <br/><br/>
                            Құрметті студент, курс пайдасын арттырып, Сіздің назарыңызды дұрыс бағытқа бұру мақсатында курстың арнайы ережелерін бекіттік.
                            <br/><br/>
                            Университет аясындағы ақпарат, тапсырмалар, тапсырманың есебін беру, сабақ кестесі, жаңалықтарды оқу ыңғайлы болу үшін, арнайы сайт (алдығы уақытта платформа) құрастырдық. Барлық сабақ тек сол платформа арқылы жүреді. Сілтемесі: https://shou.kz/
                            <br/><br/>
                            Оқу басталатын күні, бізге жіберген e-mail почтаңызға, немесе телефоныңызға смс түрінде платформаға кіруге қажетті «құпия сөзі» бар хат жібереміз. Сондықтан Ақпаратты ДҰРЫС жіберу өте маңызды! Ақпарат қате жіберілген жағдайда, платформаға уақытылы кіргеніңізге жауап бермейміз.
                            <br/><br/>
                            Айлық төлем жасап, қатысып жатқан қатысушылар, сіздер платформаға әр айға доступ аласыздар. Төлем жасалмаған жағдайда, келесі айда курс сізге қолжетімсіз болады!
                            <br/><br/>
                            Кейін почтаңызға ешнәрсе келмейді. Барлық сабақ тек платформа аясында болады. 
                            Платформаға 3-тен артық техника құралдары арқылы кірсеңіз, курс қолжетімсіз болып қалатынын ескертеміз.
                            <br/><br/>
                            Құрметті студент, курс пайдасын арттырып, Сіздің назарыңызды дұрыс бағытқа бұру мақсатында курстың арнайы ережелерін бекіттік.
                            <br/><br/>
                            1. Төлем жасалған соң, оқу ақысы мүлде қайтарылмайды!<br/>
                            2. Біз сіздің жеке ақпаратыңызды қате жазғаныңызға жауапты емеспіз, сол үшін мұқият толтыруды өтінеміз.<br/>
                            3. Барлық сабақтар, тапсырмалар Астана уақытымен беріледі/тексеріледі.<br/>
                            4. Біздің мамандарға сағат 9:00-21:00 аралығында ғана сұрақ қоюға болады.<br/>
                            5. Курс кураторлары мен администраторларын жексенбі күндері мазалауға болмайды. Телефон арқылы хабарлама жазуға да.<br/>
                            6. Ақпаратты, тапсырманы толық оқу (түсінбеген жағдайда тағы екі рет қайталап оқу)<br/>
                            7. Тапсырмаларды уақытылы орындауыңыз керек. <br/>
                            8. Сылтау қабылданбайды<br/>
                            9. Басқа қатысушыларға немесе курс ұйымдастырушыларына дөрекі сөйлеуге, дініне, жынысына, жеке басына, намысына тиесілі сөздер айтуға болмайды.<br/>
                            10. Оқу материалдарын, журналды жүктеуге, таратуға болмайды. Барлығы авторлық құқықпен қорғалған!<br/>
                            11. Барлық сұрақтарыңызды what’s app -қа xат түрінде жіберу (қоңырау шалуға, аудио жазба жіберуге болмайды)<br/>
                            12. Айлық төлем (3900тг) жасап қатысып жатқан студенттер әр айдың 25 дейін келесі айдың төлемін төлеп отыру қажет. <br/>
                            13. Әр орындалмаған ереже шарты үшін ескерту аласыз, 3 ескерту алған студент оқудан шығарылады. Ережелерін сақтамаған қатысушыларды, модератор ақшаны қайтармай курстан шығара алады.
                            Студенттер саны көп болғандықтан, қарапайым, Гууглдан іздеп табуға болатын сұрақтарға жауап бермейміз. Түсінушілікпен қарауларыңызды сұраймын. 
                            Платформаға қатысты сұрақтарыңызды +7775 071 3837 осы номерге Ватсапқа ХАТ түрінде жазыңыз.<br/>
                        </div>
                   </div>
                </div>
            </div>
        )
    }
}

export default Rules