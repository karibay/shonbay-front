import React, { Component } from 'react'
import {
    Route, 
    withRouter
} from 'react-router-dom'
import { connect } from 'react-redux'

import Body from './presentational/Body'
import Login from './presentational/Login'
import RestorePassw from './presentational/RestorePassw'
import EmailCode from './presentational/EmailCode'
import FillProfile from './presentational/FillProfile'
import ProfileSettings from './container/ProfileSettings'
import Courses from './container/Courses'
import CurrentCourse from './container/CurrentCourse'
import Rules from './container/Rules'
import Lesson from './container/Lesson'
import EditLesson from './container/EditLesson'
import LessonExample from './container/LessonExample'

class Routing extends Component {
    render() {
        return (
            <div className={ this.props.hideHeaderLeftMenu ? "" : "routing" }>
                <Route exact path="/" component={Body} />
                <Route path="/login" component={Login} />
                <Route path="/change-password" component={RestorePassw}/>
                <Route path="/email-code" component={EmailCode}/>
                <Route path="/fill-profile" component={FillProfile}/>
                <Route path="/profile-settings" component={ProfileSettings}/>
                <Route path="/courses" component={Courses}/>
                <Route exact path="/current-course/:courseId/" component={CurrentCourse}/>
                <Route path="/rules" component={Rules}/>
                <Route path="/current-course/:courseId/lesson/:lessonId" component={Lesson}/>
                <Route exact path="/current-course/:courseId/edit-lesson/:lessonId/" component={EditLesson}/>
                <Route exact path="/current-course/:courseId/edit-lesson/" component={EditLesson}/>
                <Route exact path="/lesson-example" component={LessonExample}/>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    hideHeaderLeftMenu: state.ui.hideHeaderLeftMenu
})

const mapDispatchToProps = dispatch => ({

})

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Routing))