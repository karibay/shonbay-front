import React, { Component } from 'react'
import logo from '../../assets/img/shonbay_logo_1.png'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMobileAlt, faEye } from '@fortawesome/free-solid-svg-icons'
import { bindActionCreators } from 'redux'
import * as uiActions from '../../actions/ui'
import * as userActions from '../../actions/user'
import * as courseActions from '../../actions/courses'
import InputMask from 'react-input-mask'

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            saveLogin: false
        }
        this.handleChange = this.handleChange.bind(this)
        this.phoneChange = this.phoneChange.bind(this)
        this.passwChange = this.passwChange.bind(this)
        // this.repeatPasswChange = this.repeatPasswChange.bind(this)
        this.login = this.login.bind(this)
    }

    componentDidMount() {
        if ( localStorage.getItem('shonbay_token') ) {
            this.props.history.push( '/courses' )
        }
        this.props.uiActions.HIDE_HEADER_LEFT_MENU( true )
    }
    componentWillUnmount = () => this.props.uiActions.HIDE_HEADER_LEFT_MENU( false )
    handleChange = (event) => this.setState({ saveLogin: event.target.value })
    phoneChange = (event) => this.props.userActions.SET_USER_PHONE( event.target.value )
    passwChange = (event) => this.props.userActions.SET_USER_PASSW( event.target.value )
    // repeatPasswChange(event) { this.props.userActions.SET_USER_REPEAT_PASSW( event.target.value ) }
    login = () => {
        // username, bday, instagram
        const { phone, passw } = this.props.userInfo
        this.props.userActions.LOGIN_REQUEST( phone, passw ).then((data) => {
            if ( data ) {
                localStorage.setItem('shonbay_token', data.token)
                localStorage.setItem('shonbay_user', JSON.stringify(data.user))
                this.props.userActions.SET_USER_TOKEN(data.token)
                this.props.userActions.SET_USER_ID(data.user.id)
                this.props.userActions.SET_USER_PHONE(data.user.phone_number)
                this.props.userActions.SET_USER_TYPE(data.user.user_type)
                
                this.props.userActions.GET_CURRENT_USER_REQUEST( data.token ).then((user_data) => {
                    if (user_data) {
                        if ( user_data.name.length === 0 || user_data.birth_date.length === 0 || user_data.instagram.length === 0 ) {
                            this.props.history.push(`/fill-profile`)
                        } else {
                            this.props.history.push(`/courses`)
                        }
                        this.props.userActions.SET_USER_NAME( user_data.name )
                        this.props.userActions.SET_USER_EMAIL( user_data.email )
                        this.props.userActions.SET_USER_SEX( (user_data.gender === 'male') ? 1 : 0 )
                        this.props.userActions.SET_USER_BDAY( user_data.birth_date )
                        this.props.userActions.SET_USER_INSTAGRAM( user_data.instagram )
                        this.props.userActions.SET_USER_TYPE( user_data.user_type )
                    } else {
                        alert( 'Проблемы с сервером. Невозможно получить данные пользователя' )
                    }
                })
            } else {
                alert( 'Сіздің логин немесе құпиясөз дұрыс емес' )
            }
        })
    }
    render() {
        return (
            <div className="login">
                <div className="logo">
                    <img alt="logo" src={logo}></img>
                </div>
                <div className="fs-24 dark-indigo align-center">ZRatio системасына қош келдіңіз!</div>
                <div className="login__form">
                    <div className="login__form__phone-wrap">
                        <FontAwesomeIcon icon={faMobileAlt} />
                        <InputMask
                            // {...this.props}
                            placeholder="Телефон"
                            value={this.props.userInfo.phone} 
                            onChange={this.phoneChange} 
                            mask="+79999999999" 
                            maskChar=" " />
                        {/* <input  type="text" 
                                placeholder="Телефон"
                                 ></input> */}
                    </div>
                    <div className="login__form__password-wrap">
                        <FontAwesomeIcon icon={faEye} />
                        <input  type="password" 
                                placeholder="Құпиясөз"
                                value={this.props.userInfo.passw} 
                                onChange={this.passwChange} ></input>
                    </div>
                    <div className="remember-me">
                        <label className="container">Мені есте сақта
                            <input  type="checkbox" 
                                    value={this.state.saveLogin} 
                                    onChange={this.handleChange} 
                                    checked={this.state.saveLogin} />
                            <span className="checkmark"></span>
                        </label>
                    </div>
                    <div className="submit">
                        {/* <Link to={`/courses`} style={{ width: '100%' }} > */}
                        <button className="button" 
                                type="submit"
                                onClick={ () => this.login() } >
                            Кіру
                        </button>
                        {/* </Link> */}
                    </div>
                    <div className="forget-passw fs-13 mt-24">
                        <Link to={`/change-password`} >
                            Құпиясөзді умыттың ба?
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    userInfo: state.userInfo
})

const mapDispatchToProps = dispatch => ({
    uiActions: bindActionCreators(uiActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
    courseActions: bindActionCreators(courseActions, dispatch)
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(Login))
