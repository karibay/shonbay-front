import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes, faPaperclip } from '@fortawesome/free-solid-svg-icons'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as modalActions from '../../actions/modal'
import * as courseActions from '../../actions/courses'

class DeleteCourseModal extends Component {
    constructor(props) {
        super(props)
    }
    deleteCourse() {
        this.props.courseActions.DELETE_COURSE_REQUEST(this.props.modal.addCourseInfo.courseId, this.props.userInfo.token).then((data) => {
            this.props.modalActions.CLOSE_MODAL()
            this.props.courseActions.COURSE_LIST_REQUEST(this.props.userInfo.token).then((data) => {
                if (data) {
                    this.props.courseActions.SET_COURSE_LIST(data)
                } else {
                    alert('Проблемы с сервером')
                }
            })
        })
    }
    render() {
        return (
            <div className="modal-window">
                <div className="modal-window__close">
                    <FontAwesomeIcon onClick={ _ => this.props.modalActions.CLOSE_MODAL() } className="modal-window__close-icon" icon={faTimes} />
                </div>
                <div className="modal-window__header dark-indigo fs-20">
                    Курсты жою
                </div>
                <div className="modal-window__description slate fs-14 lh-20 mt-8">
                    Курсты жоюға сенімдіcіз бе?
                </div>
                <div className="modal-window__form mt-24">
                    <div className="modal-window__form__wrap mt-18">
                        <button onClick={ _ => this.deleteCourse() } className="delete-course modal-delete-course">
                            Курсты жою
                        </button>
                        <button onClick={ _ => this.props.modalActions.CLOSE_MODAL() } className="cancel-button ml-10">
                            Жабу
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    userInfo: state.userInfo,
    modal: state.modal
})

const mapDispatchToProps = dispatch => ({
    modalActions: bindActionCreators(modalActions, dispatch),
    courseActions: bindActionCreators(courseActions, dispatch)
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(DeleteCourseModal))
