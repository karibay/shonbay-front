import React, { Component } from 'react'
// import img from '../../assets/img/img.jpg'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
// import * as uiActions from '../../actions/ui'
import * as courseActions from '../../actions/courses'
import * as modalActions from '../../actions/modal'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons'

const Item = (props) => {
    let { title, description, image, isAvailable, id, editCourse, deleteCourse, userInfo, goTo } = props
    return (
        <div onClick={ (e) => goTo(e, id, isAvailable) } className="item" >
            <div className={ "status " + ( isAvailable ? 'open' : 'close' ) }>{ isAvailable ? 'Ашық' : 'Жабық' }</div>
            <div className="img">
                <img className="img__status" alt="course" src={image}></img>
                <div    className={ "img__edit " + ((userInfo.usertype === 'admin') ? 'show' : 'hide' ) }
                        onClick={ (e) => editCourse(e, id, title, description, userInfo.token) }>
                    <FontAwesomeIcon className="img__edit__icon" icon={faPencilAlt} />
                </div>
                <div    className={ "img__delete " + ((userInfo.usertype === 'admin') ? 'show' : 'hide' ) }
                        onClick={ (e) => deleteCourse(e, id, userInfo.token) }>
                    <FontAwesomeIcon className="img__delete__icon" icon={faTrashAlt} />
                </div>
            </div>
            <div className="description">
                <div className="description-title">
                    <p className="fs-20 dark-indigo">{title}</p>
                </div>
                <div className="description-wrap blue-grey fs-14 mt-12 lh-20">
                    <p>
                        {description}
                    </p>
                </div>
            </div>
        </div>
    )
}

class CoursesList extends Component {
    constructor(props) {
        super(props)
        this.editCourse = this.editCourse.bind(this)
        this.deleteCourse = this.deleteCourse.bind(this)
        this.goTo = this.goTo.bind(this)
    }
    componentWillMount() {
        if ( this.props.userInfo.token ) {
            this.props.courseActions.COURSE_LIST_REQUEST( this.props.userInfo.token ).then((data) => {
                if (data) { this.props.courseActions.SET_COURSE_LIST( data ) }
                else { alert( 'Проблемы с сервером' ) }
            })
        }
    }
    editCourse = (e, id, title, description, token) => {
        this.props.modalActions.OPEN_MODAL('editCourse')
        this.props.modalActions.SET_COURSE_ID(id)
        this.props.modalActions.SET_COURSE_TITLE(title)
        this.props.modalActions.SET_COURSE_DESCRIPTION(description)
        e.preventDefault()
        e.stopPropagation()
    }
    deleteCourse = (e, id) => {
        this.props.modalActions.OPEN_MODAL('deleteCourse')
        this.props.modalActions.SET_COURSE_ID(id)
        e.preventDefault()
        e.stopPropagation()
    }
    goTo = (e, id, isAvailable) => { if (isAvailable) this.props.history.push(`/current-course/` + id) }
    render() {
        let elements = []
        for ( let i = 0; i < this.props.courses.courseList.length; i++ ) {
            let item = this.props.courses.courseList[i]
            elements.push( <Item
                            id={item.id}
                            key={item.id}
                            title={ item.title }
                            description={ item.description }
                            image={ item.image }
                            isAvailable={ item.is_available }
                            userInfo={ this.props.userInfo }
                            editCourse={ (e, id, title, description, token) => this.editCourse(e, id, title, description, token) }
                            deleteCourse={ (e, id, token) => this.deleteCourse(e,id, token) }
                            goTo={ (e, id, isAvailable) => this.goTo(e, id, isAvailable) } /> )
        }
        return (
            <div className="courses-list mt-30">
                {elements}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    userInfo: state.userInfo,
    courses: state.courses
})

const mapDispatchToProps = dispatch => ({
    courseActions: bindActionCreators(courseActions, dispatch),
    modalActions: bindActionCreators(modalActions, dispatch)
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(CoursesList))
