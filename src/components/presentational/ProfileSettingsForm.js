import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendarAlt, faEnvelope } from '@fortawesome/free-regular-svg-icons'
import { faMobileAlt, faEye } from '@fortawesome/free-solid-svg-icons'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../../actions/user'
import InputMask from 'react-input-mask'

class ProfileSettingsForm extends Component {
    constructor(props) {
        super(props)
        this.usernameChange = this.usernameChange.bind(this)
        this.sexChange = this.sexChange.bind(this)
        this.bdayChange = this.bdayChange.bind(this)
        this.instagramChange = this.instagramChange.bind(this)
        this.emailChange = this.emailChange.bind(this)
        this.phoneChange = this.phoneChange.bind(this)
        this.saveProfileSettings = this.saveProfileSettings.bind(this)
        this.changePassword = this.changePassword.bind(this)
        this.passwChange = this.passwChange.bind(this)
        this.repeatPasswChange = this.repeatPasswChange.bind(this)
    }
    usernameChange = (event) => { this.props.userActions.SET_USER_NAME( event.target.value ) }
    sexChange = (value) => { this.props.userActions.SET_USER_SEX( !this.props.userInfo.sex ) }
    bdayChange = (event) => { this.props.userActions.SET_USER_BDAY( event.target.value ) }
    instagramChange = (event) => { this.props.userActions.SET_USER_INSTAGRAM( event.target.value ) }
    emailChange = (event) => { this.props.userActions.SET_USER_EMAIL( event.target.value ) }
    phoneChange = (event) => { this.props.userActions.SET_USER_PHONE( event.target.value ) }
    passwChange = (event) => { this.props.userActions.SET_USER_PASSW( event.target.value ) }
    repeatPasswChange = (event) => { this.props.userActions.SET_USER_REPEAT_PASSW( event.target.value ) }
    saveProfileSettings = () => {
        const { username, sex, bday, instagram, email, phone, userId, token } = this.props.userInfo
        this.props.userActions.UPDATE_USER_REQUEST( username, sex ? 'male' : 'female', bday, instagram, email, phone, userId, token ).then((data) => {
            if (data) { alert( 'Информация сохранена' ) } 
            else { alert( 'Ошибка с сервером' ) }
            this.changePassword()
        })
    }
    changePassword = () => {
        let { passw, repeatPassw } = this.props.userInfo
        // if ( passw === repeatPassw ) {
        this.props.userActions.RESET_PASSWORD_REQUEST( passw, repeatPassw, this.props.userInfo.token ).then((data) => {
            if (data) { 
                alert( 'Пароли изменены' )
                localStorage.removeItem('shonbay_token')
                localStorage.removeItem('shonbay_user') 
                this.props.history.push('/login')
            } 
            else { alert( 'Пароли не изменены' ) }
        })
        // } else {
        //     alert( 'Пароли не совпадают' )
        // }
    }
    render() {
        return (
            <div className="profile-settings-form mt-24">
                <div className="profile-settings-form__form pt-24">
                    <div className="mb-6 lh-16 fs-12 blue-grey">Аты-жөні</div>
                    <div className="profile-settings-form__form-name">
                        <input  type="text" 
                                placeholder="Аты-жөні"
                                value={ this.props.userInfo.username }
                                onChange={ this.usernameChange }></input>
                    </div>
                    <div className="mt-18 mb-6 lh-16 fs-12 blue-grey">
                        Жынысы
                    </div>
                    <div className="sex">
                        <label className="container">Еркек
                            <input  type="checkbox" 
                                    value={this.props.userInfo.sex} 
                                    onChange={this.sexChange} 
                                    checked={this.props.userInfo.sex} />
                            <span className="checkmark-round"></span>
                        </label>
                        <label className="container ml-20">Әйел
                            <input  type="checkbox" 
                                    value={!this.props.userInfo.sex} 
                                    onChange={this.sexChange} 
                                    checked={!this.props.userInfo.sex} />
                            <span className="checkmark-round"></span>
                        </label>
                    </div>
                    <div className="mt-18 mb-6 lh-16 fs-12 blue-grey">
                        Туған күн
                    </div>
                    <div className="profile-settings-form__form-bday">
                        <FontAwesomeIcon icon={faCalendarAlt} />
                        <InputMask
                            {...this.props}
                            placeholder="1990-01-01"
                            value={this.props.userInfo.bday}
                            onChange={this.bdayChange}
                            mask="9999-99-99" 
                            maskChar="_"
                            alwaysShowMask={false} />
                        {/* <input  type="text" 
                                    placeholder="01-01-1990"
                                    value={this.props.userInfo.bday}
                                    onChange={this.bdayChange} ></input> */}
                    </div>
                    <div className="mt-18 mb-6 lh-16 fs-12 blue-grey">
                        Instagram
                    </div>
                    <div className="profile-settings-form__form-socnetw">
                        <svg    aria-hidden="true" 
                                focusable="false" 
                                data-prefix="fab" 
                                data-icon="instagram" 
                                className="svg-inline--fa fa-instagram fa-w-14" 
                                role="img" 
                                xmlns="http://www.w3.org/2000/svg" 
                                viewBox="0 0 448 512">
                                <path 
                                    fill="currentColor" 
                                    d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path></svg>
                        <input  type="text" 
                                placeholder="@"
                                value={this.props.userInfo.instagram}
                                onChange={this.instagramChange} ></input>
                    </div>
                    <div className="mt-18 mb-6 lh-16 fs-12 blue-grey">
                        Электронды пошта
                    </div>
                    <div className="profile-settings-form__form-mail">
                        <FontAwesomeIcon icon={faEnvelope} />
                        <input  type="email" 
                                placeholder="aibek@gmail.com"
                                disabled
                                value={this.props.userInfo.email}
                                onChange={this.emailChange} ></input>
                    </div>
                    <div className="line"></div>
                    <div className="mt-18 mb-6 lh-16 fs-12 blue-grey">
                        Телефон
                    </div>
                    <div className="profile-settings-form__form-phone">
                        <FontAwesomeIcon icon={faMobileAlt} />
                        <input  type="phone" 
                                placeholder="+7 (701)"
                                disabled
                                value={this.props.userInfo.phone}
                                onChange={this.phoneChange} ></input>
                    </div>
                    <div className="mt-18 mb-6 lh-16 fs-12 blue-grey">
                        Құпиясөз
                    </div>
                    <div className="profile-settings-form__form-passw">
                        <FontAwesomeIcon icon={faEye} />
                        <input  type="password" 
                                placeholder=""
                                value={this.props.userInfo.passw}
                                onChange={this.passwChange} ></input>
                    </div>
                    <div className="mt-18 mb-6 lh-16 fs-12 blue-grey">
                        Жаңа құпиясөз
                    </div>
                    <div className="profile-settings-form__form-new-passw">
                        <FontAwesomeIcon icon={faEye} />
                        <input  type="password" 
                                placeholder=""
                                value={this.props.userInfo.repeatPassw}
                                onChange={this.repeatPasswChange} ></input>
                    </div>
                    <div className="submit">
                        <button onClick={this.saveProfileSettings} >
                            Сақтау
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    userInfo: state.userInfo
})

const mapDispatchToProps = dispatch => ({
    userActions: bindActionCreators(userActions, dispatch)
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfileSettingsForm))
