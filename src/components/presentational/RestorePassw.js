import React, { Component } from 'react'
import logo from '../../assets/img/shonbay_logo_1.png'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faMobileAlt } from '@fortawesome/free-solid-svg-icons'
import { faEnvelope } from '@fortawesome/free-regular-svg-icons'
import { bindActionCreators } from 'redux'
import * as uiActions from '../../actions/ui'
import * as userActions from '../../actions/user'

class RestorePassw extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: ''
        }
        this.sendRequest = this.sendRequest.bind(this)
    }
    componentWillMount() {
        this.props.uiActions.HIDE_HEADER_LEFT_MENU( true )
    }
    componentWillUnmount() {
        this.props.uiActions.HIDE_HEADER_LEFT_MENU( false )
    }
    sendRequest = () => {
        this.props.userActions.RESTORE_PASSW_REQUEST( this.state.email ).then((data) => {
            if (data) {
                alert( 'Сіздің поштаңызға тексеру коды жіберілді' )
                localStorage.setItem( 'temporaryEmail', this.state.email )
                this.props.history.push( '/email-code' )
            } else {
                alert( 'Ошибка сервера' )
            }
        })
    }
    render() {
        return (
            <div className="restore-passw">
                <div className="logo">
                    <img alt="logo" src={logo}></img>
                </div>
                <div className="fs-20 fw-600 dark-indigo align-center">Құпиясөзді қалпына келтіру</div>
                <div className="fs-14 blue-grey mt-10 align-center">Құпиясөзді қалпына келтіру үшін, тіркелген телефон нөмірін енгізіңіз</div>
                <form className="restore-passw__form mt-40">
                    <div className="restore-passw__form__phone-wrap">
                        <FontAwesomeIcon icon={faEnvelope} />
                        <input  type="text" 
                                placeholder="Email" 
                                value={this.state.email}
                                onChange={ (e) => this.setState({ email: e.target.value }) } ></input>
                    </div>
                    <div className="submit">
                        {/* <Link to={`/email-code`} style={{ width: '100%' }} > */}
                            <button type="submit" 
                                    onClick={ _ => this.sendRequest() }>
                                Қалпына келтіру
                            </button>
                        {/* </Link> */}
                    </div>
                    <div className="returnToAuth fs-13 mt-24">
                        <Link to={`/login`} >
                            Жүйеге кіру
                        </Link>
                    </div>
                </form>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    
})

const mapDispatchToProps = dispatch => ({
    uiActions: bindActionCreators(uiActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch)
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(RestorePassw))