import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as modalActions from '../../actions/modal'

class TaskFinishedModal extends Component {
    render() {
        return (
            <div className="modal-window">
                <div className="modal-window__close">
                    <FontAwesomeIcon onClick={ _ => this.props.modalActions.CLOSE_MODAL() } className="modal-window__close-icon" icon={faTimes} />
                </div>
                <div className="modal-window__header dark-indigo fs-20">
                    Рахмет!
                </div>
                <div className="modal-window__description slate fs-14 lh-20 mt-8">
                    Орындалған тапсырманыз кураторларға тексеріске жіберілді. Нәтижесі электронды поштаңызға жіберіледі.
                </div>
                <div className="modal-window__form mt-24">
                    
                    <div className="modal-window__form__wrap mt-18">
                        <button onClick={ _ => {
                            this.props.history.goBack()
                            this.props.modalActions.CLOSE_MODAL()
                        } } className="next-lesson">
                            Келесі сабаққа өтуw
                        </button>
                        <button onClick={ _ => {
                            this.props.history.goBack()
                            this.props.modalActions.CLOSE_MODAL()
                        } } className="cancel-button ml-10">
                            Жабу
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    // isModalOpened: state.modal.opened
})

const mapDispatchToProps = dispatch => ({
    modalActions: bindActionCreators(modalActions, dispatch)
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(TaskFinishedModal))