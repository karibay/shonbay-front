import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes, faPaperclip } from '@fortawesome/free-solid-svg-icons'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as modalActions from '../../actions/modal'
import * as courseActions from '../../actions/courses'
import { withTranslation } from 'react-i18next'

class ModalWindow extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isAvailable: false,
            file: '',
            title: '',
            desc: '',
        }
        this.changeToggle = this.changeToggle.bind(this)
        this.addCourse = this.addCourse.bind(this)
    }
    changeToggle() {
        this.setState({
            isAvailable: !this.state.isAvailable
        })
        console.log(this.formData)
    }
    addCourse() {
        let { file, title, desc, isAvailable } = this.state
        let formData = new FormData()
        formData.append("image", file)
        formData.append("title", title)
        formData.append("description", desc)
        formData.append("is_available", isAvailable)
        this.props.courseActions.CREATE_COURSE_REQUEST( formData, this.props.userInfo.token ).then((data) => {
            if (data) {
                alert('Курс успешно добавлен')
                this.props.modalActions.CLOSE_MODAL()
                this.props.courseActions.COURSE_LIST_REQUEST(this.props.userInfo.token).then((data) => {
                    if (data) {
                        this.props.courseActions.SET_COURSE_LIST(data)
                        this.setState({
                            isAvailable: false,
                            file: '',
                            title: '',
                            desc: '',
                        })
                    } else {
                        alert('Проблемы с сервером')
                    }
                })
            } else {
                alert( 'Ошибка с сервером' )
            }
        })
    }
    render() {
        const { t } = this.props;
        return (
            <div className="modal-window">
                <div className="modal-window__close">
                    <FontAwesomeIcon onClick={ _ => this.props.modalActions.CLOSE_MODAL() } className="modal-window__close-icon" icon={faTimes} />
                </div>
                <div className="modal-window__header dark-indigo fs-20">
                    { t('addCourse') }
                </div>
                <div className="modal-window__description slate fs-14 lh-20 mt-8">
                    { t('addCourseHint') }
                </div>
                <div className="modal-window__form mt-24">
                    <div className="modal-window__form__wrap">
                        <input  value={this.state.title}
                                onChange={(e) => this.setState({ title: e.target.value })}
                                ></input>
                    </div>
                    <div className="modal-window__form__wrap mt-18">
                        <textarea
                            value={this.state.desc}
                            onChange={(e) => this.setState({ desc: e.target.value })}
                            >

                        </textarea>
                    </div>
                    <div className="modal-window__form__wrap mt-18">
                        <input  type="file"
                                style={{width: '360px'}}
                                onChange={ (data) => {
                                    try {
                                        let img, file = data.target.files[0]
                                        img = new Image()
                                        img.src = window.URL.createObjectURL(data.target.files[0])
                                        const scope = this
                                        img.onload = function() {
                                            window.URL.revokeObjectURL(img.src)
                                            scope.setState({file})
                                            alert( 'Картинка успешно загружена' )
                                        }
                                    } catch(error) {
                                        alert( 'Картинка не загружена' )
                                    }
                                } }></input>
                        {/* <button className="button delete-button">
                            <FontAwesomeIcon className="trash-icon" icon={faTrashAlt} />
                        </button> */}
                        <button className="button attach-button">
                            <FontAwesomeIcon className="attach-icon" icon={faPaperclip} />
                        </button>
                    </div>
                    <div className="modal-window__form__wrap mt-18 fs-12 slate">
                        { t('statusOfCourse') }
                    </div>
                    <div className="modal-window__form__wrap mt-8 slate fs-14">
                        <label className="switch">
                            <input  type="checkbox"
                                    checked={this.state.isAvailable}
                                    />
                            <span   className="slider round"
                                    onClick={() => this.changeToggle()}></span>
                        </label>
                        <div className="ml-14">
                            { t('open') }
                        </div>
                    </div>
                    <div className="modal-window__form__wrap mt-18">
                        <button onClick={ _ => this.addCourse() } className="add-course modal-add-course">
                            { t('addCourseButtonText') }
                        </button>
                        <button onClick={ _ => this.props.modalActions.CLOSE_MODAL() } className="cancel-button ml-10">
                            { t('close') }
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    userInfo: state.userInfo
})

const mapDispatchToProps = dispatch => ({
    modalActions: bindActionCreators(modalActions, dispatch),
    courseActions: bindActionCreators(courseActions, dispatch)
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(withTranslation()(ModalWindow)))
