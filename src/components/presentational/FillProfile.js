import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as uiActions from '../../actions/ui'
import * as userActions from '../../actions/user'
import InputMask from 'react-input-mask'

class FillProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            sex: [true,false]
        }
        this.usernameChange = this.usernameChange.bind(this)
        this.sexChange = this.sexChange.bind(this)
        this.bdayChange = this.bdayChange.bind(this)
        this.instagramChange = this.instagramChange.bind(this)
        this.saveProfileSettings = this.saveProfileSettings.bind(this)
    }
    componentWillMount() {
        this.props.uiActions.HIDE_HEADER_LEFT_MENU( true )
    }
    componentWillUnmount() {
        this.props.uiActions.HIDE_HEADER_LEFT_MENU( false )
    }
    usernameChange(event) { this.props.userActions.SET_USER_NAME( event.target.value ) }
    sexChange(value) { this.props.userActions.SET_USER_SEX( !this.props.userInfo.sex ) }
    bdayChange(event) { this.props.userActions.SET_USER_BDAY( event.target.value ) }
    instagramChange(event) { this.props.userActions.SET_USER_INSTAGRAM( event.target.value ) }
    saveProfileSettings() {
        const { username, sex, bday, instagram, email, phone, userId, token } = this.props.userInfo
        this.props.userActions.UPDATE_USER_REQUEST( username, sex ? 'male' : 'female', bday, instagram, email, phone, userId, token ).then((data) => {
            if (data) {
                alert( 'Информация сохранена' )
                this.props.history.push('/courses')
            } else {
                alert( 'Ошибка с сервером' )
            }
        })
    }
    render() {
        return (
            <div className="fill-profile">
                <div className="pt-40 fs-20 fw-600 dark-indigo align-center">Анкетаны толтырыңыз</div>
                <div className="fs-14 blue-grey mt-10 align-center">Сізбен жақынырақ танысу үшін, анкетадағы барлық жиектерді толтырыңыз</div>
                <div className="fill-profile__form mt-40">
                    <div className="mb-6 lh-16 fs-12 blue-grey">Аты-жөні</div>
                    <div>
                        <input  type="text" 
                                placeholder="Аты-жөні"
                                value={ this.props.userInfo.username }
                                onChange={ this.usernameChange } ></input>
                    </div>
                    <div className="mt-24 mb-6 lh-16 fs-12 blue-grey">
                        Жынысы
                    </div>
                    <div className="sex">
                        <label className="container">Еркек
                            <input  type="checkbox" 
                                    value={this.props.userInfo.sex} 
                                    onChange={this.sexChange} 
                                    checked={this.props.userInfo.sex} />
                            <span className="checkmark-round"></span>
                        </label>
                        <label className="container ml-20">Әйел
                            <input  type="checkbox" 
                                    value={!this.props.userInfo.sex} 
                                    onChange={this.sexChange} 
                                    checked={!this.props.userInfo.sex} />
                            <span className="checkmark-round"></span>
                        </label>
                    </div>
                    <div className="mt-24 mb-6 lh-16 fs-12 blue-grey">
                        Туған күн
                    </div>
                    <div>
                        {/* <input  type="text" 
                                placeholder="01 / 01 / 1990"
                                value={this.props.userInfo.bday}
                                onChange={this.bdayChange} ></input> */}
                        <InputMask
                            {...this.props}
                            placeholder="1990-01-01"
                            value={this.props.userInfo.bday}
                            onChange={this.bdayChange}
                            mask="9999-99-99" 
                            maskChar="_"
                            alwaysShowMask={false} />
                    </div>
                    <div className="mt-24 mb-6 lh-16 fs-12 blue-grey">
                        Instagram
                    </div>
                    <div>
                        <input  type="text" 
                                placeholder="@"
                                value={this.props.userInfo.instagram}
                                onChange={this.instagramChange} ></input>
                    </div>
                    <div className="submit">
                        {/* <Link style={{width: '100%'}} to={`/login`} > */}
                        <button type="submit"
                                onClick={this.saveProfileSettings} >
                            Жіберу
                        </button>
                        {/* </Link> */}
                    </div>
                    {/* <div className="forget-passw fs-13 mt-24">
                        <Link to={`/courses`} >
                            Кейін толтыру
                        </Link>
                    </div> */}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    userInfo: state.userInfo
})

const mapDispatchToProps = dispatch => ({
    uiActions: bindActionCreators(uiActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch)
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(FillProfile))