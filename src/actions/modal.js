import * as types from '../constants'

/**
 * Open modal action
 * @param {*} modalName 
 */
export function OPEN_MODAL(modalName) {
    return dispatch => {
        dispatch({
            type: types.OPEN_MODAL,
            modalName
        })
    }
}

/**
 * Close modal action
 */
export function CLOSE_MODAL() {
    return dispatch => {
        dispatch({
            type: types.CLOSE_MODAL
        })
    }
}

/**
 * Set add course info form data to create new course
 * @param {object} addCourseInfo 
 */
export function SET_ADD_COURSE_INFO(addCourseInfo) {
    return dispatch => {
        dispatch({
            type: types.SET_ADD_COURSE_INFO,
            addCourseInfo
        })
    }
}

/**
 * 
 * @param {number} courseId 
 */
export function SET_COURSE_ID(courseId) {
    return dispatch => {
        dispatch({
            type: types.SET_COURSE_ID,
            courseId
        })
    }
}

export function SET_COURSE_TITLE(title) {
    console.log(title, 'set course title')
    return dispatch => {
        dispatch({
            type: types.SET_COURSE_TITLE,
            title
        })
    }
}

export function SET_COURSE_DESCRIPTION(description) {
    return dispatch => {
        dispatch({
            type: types.SET_COURSE_DESCRIPTION,
            description
        })
    }
}