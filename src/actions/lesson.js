import * as types from '../constants'
// import axios from 'axios'
// import { url, api } from '../path.json'

// const URL = url
// const ContentType = {
//     'Content-Type': 'application/json'
// }
// const ContentTypeImage = {
//     'Content-Type': 'application/x-www-form-urlencoded'
// }

// Redux
export function SET_LESSON_TITLE( title ) {
    return dispatch => {
        dispatch({
            type: types.SET_LESSON_TITLE,
            title
        })
    }
}

export function SET_LESSON_DESC( description ) {
    return dispatch => {
        dispatch({
            type: types.SET_LESSON_DESC,
            description
        })
    }
}

export function SET_LESSON_MEDIA( media ) {
    return dispatch => {
        dispatch({
            type: types.SET_LESSON_MEDIA,
            media
        })
    }
}

export function SET_LESSON_TASKS( tasks ) {
    return dispatch => {
        dispatch({
            type: types.SET_LESSON_TASKS,
            tasks
        })
    }
}

export function SET_LESSON_LOAD_FILE(  ) {
    return dispatch => {
        dispatch({
            type: types.SET_LESSON_LOAD_FILE
        })
    }
}

export function SET_LESSON_IMAGE( lessonImage ) {
    return dispatch => {
        dispatch({
            type: types.SET_LESSON_IMAGE,
            lessonImage
        })
    }
}

export function SET_MIN_TIME( minTime ) {
    return dispatch => {
        dispatch({
            type: types.SET_MIN_TIME,
            minTime
        })
    }
}

export function SET_MAX_TIME( maxTime ) {
    return dispatch => {
        dispatch({
            type: types.SET_MAX_TIME,
            maxTime
        })
    }
}

export function SET_MODIFIED_AT( modified_at ) {
    return dispatch => {
        dispatch({
            type: types.SET_MODIFIED_AT,
            modified_at
        })
    }
}

export function SET_LESSON_ID( lessonId ) {
    return dispatch => {
        dispatch({
            type: types.SET_LESSON_ID,
            lessonId
        })
    }
}

export function SET_STATUS( status ) {
    return dispatch => {
        dispatch({
            type: types.SET_STATUS,
            status
        })
    }
}