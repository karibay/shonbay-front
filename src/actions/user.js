import * as types from '../constants'
import axios from 'axios'
import { url, api } from '../path.json'

const URL = url
const ContentType = {
    'Content-Type': 'application/json'
}

/**
 * Creating new user
 * @param {string} email 
 * @param {string} password 
 */
export function CREATE_USER_REQUEST( email, password ) {
    return dispatch => {
        return axios({
            method: 'post',
			url: URL + api + '/users/',
			data: {
                email, password
			},
			headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}

/**
 * Update user info
 * @param {string} name 
 * @param {bool} gender 
 * @param {string} birth_date 
 * @param {string} instagram 
 * @param {number} user_id 
 */
export function UPDATE_USER_REQUEST( name, gender, birth_date, instagram, email, phone, user_id, auth_token ) {
    ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'put',
            url: URL + api + '/users/' + user_id + '/',
            data: {
                name, gender, birth_date, instagram, email, phone
            },
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}

/**
 * Update user password
 * @param {string} code 
 */
export function NEW_PASSWORD_REQUEST( code, user_id ) {
    return dispatch => {
        return axios({
            method: 'post',
            url: URL + api + '/users/' + user_id + '/activate/',
            data: {
                code
            },
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}

/**
 * 
 * @param {string} phone_number 
 * @param {string} password 
 */
export function LOGIN_REQUEST( phone_number, password ) {
    return dispatch => {
        return axios({
            method: 'post',
            url: URL + api + '/users/login/',
            data: {
                phone_number, password
            },
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            localStorage.removeItem('shonbay_token')
            localStorage.removeItem('shonbay_user')
            console.log( 'Error:', error )
        })
    }
}

/**
 * 
 * @param {string} auth_token 
 */
export function GET_CURRENT_USER_REQUEST( auth_token ) {
    ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'get',
            url: URL + api + '/users/me/',
            data: {
                
            },
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}


export function RESTORE_PASSW_REQUEST( email ) {
    // ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'post',
            url: URL + api + '/users/request_code/',
            data: {
                email
            },
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}


export function CONFIRM_CHANGE_PASSW_REQUEST( code, email ) {
    // ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'post',
            url: URL + api + '/users/confirm_code/',
            data: {
                code, email
            },
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}

/**
 * 
 * @param {*} password 
 */
export function RESET_PASSWORD_REQUEST( password, newPassword, auth_token ) {
    ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'post',
            url: URL + api + '/users/set_password/',
            data: {
                old_password: password,
                password: newPassword
            },
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}


// REDUX ACTIONS


export function SET_USER_TYPE( usertype ) {
    return dispatch => {
        dispatch({
            type: types.SET_USER_TYPE,
            usertype
        })
    }
}

/**
 * Set username on redux
 * @param {string} username 
 */
export function SET_USER_NAME( username ) {
    return dispatch => {
        dispatch({
            type: types.SET_USER_NAME,
            username
        })
    }
}

/**
 * 
 * @param {bool} sex 
 */
export function SET_USER_SEX( sex ) {
    return dispatch => {
        dispatch({
            type: types.SET_USER_SEX,
            sex
        })
    }
}

/**
 * 
 * @param {string} bday 
 */
export function SET_USER_BDAY( bday ) {
    return dispatch => {
        dispatch({
            type: types.SET_USER_BDAY,
            bday
        })
    }
}

/**
 * 
 * @param {string} instagram 
 */
export function SET_USER_INSTAGRAM( instagram ) {
    return dispatch => {
        dispatch({
            type: types.SET_USER_INSTAGRAM,
            instagram
        })
    }
}

/**
 * 
 * @param {string} email 
 */
export function SET_USER_EMAIL( email ) {
    return dispatch => {
        dispatch({
            type: types.SET_USER_EMAIL,
            email
        })
    }
}

/**
 * 
 * @param {string} phone 
 */
export function SET_USER_PHONE( phone ) {
    return dispatch => {
        dispatch({
            type: types.SET_USER_PHONE,
            phone
        })
    }
}

/**
 * 
 * @param {string} passw 
 */
export function SET_USER_PASSW( passw ) {
    return dispatch => {
        dispatch({
            type: types.SET_USER_PASSW,
            passw
        })
    }
}

/**
 * 
 * @param {string} repeatPassw 
 */
export function SET_USER_REPEAT_PASSW( repeatPassw ) {
    return dispatch => {
        dispatch({
            type: types.SET_USER_REPEAT_PASSW,
            repeatPassw
        })
    }
}

/**
 * 
 * @param {string} token 
 */
export function SET_USER_TOKEN( token ) {
    return dispatch => {
        dispatch({
            type: types.SET_USER_TOKEN,
            token
        })
    }
}

/**
 * 
 * @param {number} userId 
 */
export function SET_USER_ID( userId ) {
    return dispatch => {
        dispatch({
            type: types.SET_USER_ID,
            userId
        })
    }
}