import * as types from '../constants'

export function SHOW_LEFT_MENU() {
    return dispatch => {
        dispatch({
            type: types.SHOW_LEFT_MENU_MOBILE
        })
    }
}

export function HIDE_HEADER_LEFT_MENU(hideHeaderLeftMenu) {
    return dispatch => {
        dispatch({
            type: types.HIDE_HEADER_LEFT_MENU,
            hideHeaderLeftMenu
        })
    }
}