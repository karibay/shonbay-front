import * as types from '../constants'
import axios from 'axios'
import { url, api } from '../path.json'

const URL = url
const ContentType = {
    'Content-Type': 'application/json'
}
const ContentTypeImage = {
    'Content-Type': 'application/x-www-form-urlencoded'
}

/**
 * 
 * @param {*} auth_token 
 */
export function COURSE_LIST_REQUEST( auth_token ) {
    ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'get',
            url: URL + api + '/courses/',
            data: {},
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}

/**
 * 
 * @param {*} image 
 * @param {*} title 
 * @param {*} description 
 */
export function CREATE_COURSE_REQUEST( formData, auth_token ) {
    ContentTypeImage.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'post',
            url: URL + api + '/courses/',
            data: formData,
            headers: ContentTypeImage
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}

/**
 * 
 * @param {number} courseId 
 * @param {string} auth_token 
 */
export function GET_COURSE_REQUEST( courseId, auth_token ) {
    ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'get',
            url: URL + api + '/courses/' + courseId + '/',
            data: {},
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}

/**
 * 
 * @param {number} courseId 
 * @param {string} auth_token 
 */
export function UPDATE_COURSE_REQUEST( formData, courseId, auth_token ) {
    ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'put',
            url: URL + api + '/courses/' + courseId + '/',
            data: formData,
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}

/**
 * 
 * @param {number} courseId 
 * @param {string} auth_token 
 */
export function DELETE_COURSE_REQUEST( courseId, auth_token ) {
    ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'delete',
            url: URL + api + '/courses/' + courseId + '/',
            data: {},
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}

/**
 * 
 * @param {*} courseId 
 * @param {*} auth_token 
 */
export function GET_COURSE_LESSONS_REQUEST( courseId, auth_token ) {
    ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'get',
            url: URL + api + '/courses/' + courseId + '/lessons/',
            data: {},
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}

/**
 * 
 * @param {number} courseId 
 * @param {string} auth_token 
 */
export function CREATE_COURSE_LESSON_REQUEST( courseId, formData, auth_token ) {
    ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'post',
            url: URL + api + '/courses/' + courseId + '/lessons/',
            data: formData,
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}

/**
 * 
 * @param {number} courseId 
 * @param {number} lessonId 
 * @param {string} auth_token 
 */
export function GET_LESSON_REQUEST( courseId, lessonId, auth_token ) {
    ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'get',
            url: URL + api + '/courses/' + courseId + '/lessons/' + lessonId + '/',
            data: {},
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}

/**
 * 
 * @param {number} courseId 
 * @param {number} lessonId 
 * @param {string} auth_token 
 */
export function UPDATE_LESSON_REQUEST( formData, courseId, lessonId, auth_token ) {
    ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'put',
            url: URL + api + '/courses/' + courseId + '/lessons/' + lessonId + '/',
            data: formData,
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}

/**
 * 
 * @param {*} formData 
 * @param {*} courseId 
 * @param {*} lessonId 
 * @param {*} auth_token 
 */
export function UPDATE_MEDIA_REQUEST( media, courseId, lessonId, auth_token ) {
    ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'post',
            url: URL + api + '/courses/' + courseId + '/lessons/' + lessonId + '/media/',
            data: media,
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}


export function DELETE_LESSON_REQUEST( courseId, lessonId, auth_token ) {
    ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'delete',
            url: URL + api + '/courses/' + courseId + '/lessons/' + lessonId + '/',
            data: {},
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}

/**
 * 
 * @param {*} courseId 
 * @param {*} lessonId 
 * @param {*} auth_token 
 */
export function SUBMIT_LESSON_REQUEST( formData, courseId, lessonId, auth_token ) {
    ContentType.Authorization = 'Token ' + auth_token
    return dispatch => {
        return axios({
            method: 'post',
            url: URL + api + '/courses/' + courseId + '/lessons/' + lessonId + '/submit/',
            data: formData,
            headers: ContentType
        }).then(response => {
            let data = response.data
            return data
        }).catch(error => {
            console.log( 'Error:', error )
        })
    }
}


// REDUX ACTIONS

/**
 * 
 * @param {*} courseList 
 */
export function SET_COURSE_LIST( courseList ) {
    return dispatch => {
        dispatch({
            type: types.SET_COURSE_LIST,
            courseList
        })
    }
}

/**
 * 
 * @param {*} courseInfo 
 */
export function SET_COURSE_INFO( courseInfo ) {
    return dispatch => {
        dispatch({
            type: types.SET_COURSE_INFO,
            courseInfo
        })
    }
}

export function SET_LESSON_LIST( lessonList ) {
    return dispatch => {
        dispatch({
            type: types.SET_LESSON_LIST,
            lessonList
        })
    }
}